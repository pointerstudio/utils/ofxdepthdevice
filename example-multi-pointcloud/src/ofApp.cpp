#include "ofApp.h"

#include <glm/gtx/matrix_decompose.hpp>
//--------------------------------------------------------------
void ofApp::setup(){

    gui.setup("", "settings.json", 20, 20);
    ofJson guiSettings = ofLoadJson("settings.json");
    string profilerJson = ofGetTimestampString() + "_result.json";
    OFX_PROFILER_BEGIN_SESSION("kirnects", profilerJson);

    ofParameterGroup appGroup;
    appGroup.setName("app_parameters");
    appGroup.add(appParameters.updateDevices.set("update_devices", true));
    appGroup.add(appParameters.drawDevices.set("draw_devices", true));
    appGroup.add(appParameters.drawDebug.set("draw_debug", true));
    ofAddListener(appGroup.parameterChangedE(), this, &ofApp::appParametersListener);
    gui.add(appGroup);
    const int32_t device_count = k4a::device::get_installed_count();
    ofParameterGroup groups[device_count];
    for(int i = 0; i < device_count; i++){
        // setup kinects
        auto kirnect = make_shared <KinectMT>();
        kirnect->setup(i);
        string serial_number = kirnect->getSerialNumber();
        cout << kirnect->getSerialNumber();
        stringstream ss_name;
        ss_name << "device_" << ofToString(i);


        kirnect->device_index = i;
        cout << "setup kinect " << i << endl;
        kirnects.push_back(kirnect);

        // setup gui
        ofParameterGroup group;
        DeviceParameters dp;

        int position_index = i;

        ofJson o = guiSettings["group"];
        if(o.find(ss_name.str()) != o.end()){
            ofJson s = o[ss_name.str()];
            if(s.find("position_index") != s.end()){
                position_index = ofToInt(s["position_index"].get <string>());
            }
        }

        group.setName(ss_name.str());
        group.add(dp.draw_device.set("draw_device", true));
        group.add(dp.draw_debug.set("draw_debug", true));
        group.add(dp.position_index.set("position_index", position_index, 0, device_count));
        group.add(dp.center.set("center", glm::vec3(0), glm::vec3(-500), glm::vec3(500)));
        group.add(dp.centerFine.set("centerFine", glm::vec3(0), glm::vec3(-10), glm::vec3(10)));
        group.add(dp.position.set("position", glm::vec3(0), glm::vec3(-500), glm::vec3(500)));
        group.add(dp.positionFine.set("positionFine", glm::vec3(0), glm::vec3(-10), glm::vec3(10)));
        group.add(dp.orientation.set("orientation", glm::vec3(0), glm::vec3(-360), glm::vec3(360)));
        group.add(dp.orientationFine.set("orientationFine", glm::vec3(0), glm::vec3(-45), glm::vec3(45)));

        ofAddListener(group.parameterChangedE(), this, &ofApp::deviceParametersListener);
        groups[position_index] = group;
        gui.add(group);
        gui.getGroup(ss_name.str()).minimizeAll();
        gui.getGroup(ss_name.str()).minimize();
        deviceParameters.push_back(dp);
    }

    gui.loadFromFile("settings.json");

    cam.setPosition(200, 200, 200);
    cam.lookAt(glm::vec3(0, 0, 0));
//    lc_enabledDevice.set(0);
//    lcListener();
//    lc_enabledDevice.newListener([&](int &){
//        return this->lcListener();
//    });
}

void ofApp::lcListener(){
//    int device_index = lc_enabledDevice.get();
//    cout << "enable device " << ofToString(device_index) << endl;
//    auto & dp = deviceParameters[device_index];
//    lc.clear();
//    lc.setup();
//    lc.radio(0, 5, lc_enabledDevice, ofxLCLeds::Red, ofxLCLeds::Green);
//    lc.toggle(6, dp.draw_device);
//    lc.toggle(7, dp.draw_debug);
//    lc.knob3(0, dp.center, dp.center.getMin(), dp.center.getMax());
//    lc.knob3(3, dp.position, dp.position.getMin(), dp.position.getMax());
//    lc.knob3(6, dp.orientation, dp.orientation.getMin(), dp.orientation.getMax());
//    lc.knob3(4, dp.position, dp.position.getMin(), dp.position.getMax());
}

void ofApp::appParametersListener(ofAbstractParameter & e){
    if(e.getName() == "update_devices"){
        bool v = e.cast <bool>();
        for(auto & k : kirnects){
            k->setUpdating(v);
        }
    }
    if(e.getName() == "draw_devices"){
        bool v = e.cast <bool>();
        for(auto & p : deviceParameters){
            p.draw_device.set(v);
        }
    }
    if(e.getName() == "draw_debug"){
        bool v = e.cast <bool>();
        for(auto & p : deviceParameters){
            p.draw_debug.set(v);
        }
    }
}

void ofApp::deviceParametersListener(ofAbstractParameter & e){
    int hierarchy_device = 1;
    string deviceString = e.getGroupHierarchyNames()[hierarchy_device];
    string deviceIndexString = deviceString;
    ofStringReplace(deviceIndexString, "device_", "");
    int deviceIndex = ofToInt(deviceIndexString);

    cout << "received gui change for device " << ofToString(deviceIndex) << " device string: " << deviceString << endl;
    const vector <string> extrinsics{"center", "position", "orientation", "centerFine", "positionFine", "orientationFine"};
    if(find(extrinsics.begin(), extrinsics.end(), e.getName()) != extrinsics.end()){
        deviceParameters[deviceIndex].setDeviceExtrinsics(kirnects[deviceIndex]);
    }
}

void DeviceParameters::setDeviceExtrinsics(shared_ptr <ofxDepthDevice::DepthDevice> & device){
    ofNode & e = device->extrinsics;
//    e.setPosition(0, 0, 0);
//    e.setOrientation(glm::quat());
    e.resetTransform();

    glm::mat4 transformation; // your transformation matrix.

    transformation = glm::translate(transformation, center.get());
    transformation = glm::translate(transformation, centerFine.get());

//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().x, glm::vec3(1, 0, 0));
//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().y, glm::vec3(0, 1, 0));
//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().z, glm::vec3(0, 0, 1));

    transformation = glm::rotate(transformation, glm::radians(orientation.get().x), glm::vec3(1, 0, 0));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().x), glm::vec3(1, 0, 0));
    transformation = glm::rotate(transformation, glm::radians(orientation.get().y), glm::vec3(0, 1, 0));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().y), glm::vec3(0, 1, 0));
    transformation = glm::rotate(transformation, glm::radians(orientation.get().z), glm::vec3(0, 0, 1));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().z), glm::vec3(0, 0, 1));

    transformation = glm::translate(transformation, -center.get());
    transformation = glm::translate(transformation, -centerFine.get());
    transformation = glm::translate(transformation, position.get());
    transformation = glm::translate(transformation, positionFine.get());

    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(transformation, scale, rotation, translation, skew, perspective);
    e.setPosition(translation);
    e.setOrientation(rotation);

    cout << "set device extrinsics " << ofToString(device->device_index) << " tx " << ofToString(translation.x) << endl;
}
void DeviceParameters::drawDebug(shared_ptr <ofxDepthDevice::DepthDevice> device){
    ofPushStyle();
    ofSetColor(ofColor::lightBlue);
    ofDrawBox(center.get(), 2);
    ofSetColor(ofColor::lightPink);
    ofDrawBox(position.get(), 2);
    device->extrinsics.transformGL();
    ofDrawAxis(300);
    device->extrinsics.restoreTransformGL();
    ofPopStyle();
}

void ofApp::changedPosition(glm::vec3 & e){
    cout << e.x << endl;
    kirnects[0]->extrinsics.setPosition(e);
//do something
}

//--------------------------------------------------------------
void ofApp::update(){
    OFX_PROFILER_FUNCTION();
    for(auto & kirnect : kirnects){
        kirnect->update();
        kirnect->draw();
//        deviceParameters[kirnect->device_index].setDeviceExtrinsics(kirnect);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    OFX_PROFILER_FUNCTION();
    ofBackground(ofColor::red);
    ofEnableDepthTest();
    int i = 0;
    for(auto & kirnect : kirnects){
        auto & p = deviceParameters[i];
        cam.begin();
        if(p.draw_device.get()){
            kirnect->show();
            ofDrawAxis(500);
        }
        if(p.draw_debug.get()){
            p.drawDebug(kirnect);
        }
        cam.end();
        i++;
    }
    ofDisableDepthTest();
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::exit(){
    OFX_PROFILER_END_SESSION();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if(key == 'u'){
        for(auto & kirnect : kirnects){
            kirnect->toggleUpdating();
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
