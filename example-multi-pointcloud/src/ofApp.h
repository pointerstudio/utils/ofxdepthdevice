#pragma once

#include "ofMain.h"
#include "ofxDepthDevice.h"
#include "ofxGui.h"

class DeviceParameters {
    public:
        ofParameter <bool> draw_device;
        ofParameter <bool> draw_debug;
        ofParameter <int> position_index;
        ofParameter <glm::vec3> center;
        ofParameter <glm::vec3> centerFine;
        ofParameter <glm::vec3> position;
        ofParameter <glm::vec3> positionFine;
        ofParameter <glm::vec3> orientation;
        ofParameter <glm::vec3> orientationFine;
        void setDeviceExtrinsics(shared_ptr <ofxDepthDevice::DepthDevice> & device);
        void drawDebug(shared_ptr <ofxDepthDevice::DepthDevice> device);
};

struct AppParameters {
    ofParameter <bool> updateDevices;
    ofParameter <bool> drawDevices;
    ofParameter <bool> drawDebug;
};

class ofApp : public ofBaseApp {

    public:
        void setup();
        void update();
        void draw();
        void exit();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        void lcListener();
        ofParameter <int> lc_enabledDevice;

        void changedPosition(glm::vec3 & e);
        void deviceParametersListener(ofAbstractParameter & e);
        void appParametersListener(ofAbstractParameter & e);

        AppParameters appParameters;

        vector <shared_ptr <ofxDepthDevice::DepthDevice> > kirnects;
//        ofxLaunchControl lc;
        ofxPanel gui;
        vector <DeviceParameters> deviceParameters;

        ofEasyCam cam;


};
