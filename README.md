# ofxDepthDevice

An ofxAddon that allows you to use [Azure Kinect](https://azure.microsoft.com/en-us/services/kinect-dk/) in [openFrameworks](https://github.com/openframeworks/openFrameworks).

## Installation

The instructions below are based on the [Azure Kinect Sensor SDK Usage](https://github.com/microsoft/Azure-Kinect-Sensor-SDK/blob/develop/docs/usage.md) page.

### Linux

* Clone this repository in your openFrameworks `addons` folder.
* Then run the install script
```
$ cd <ofxDepthDevice directory>/scripts/ci/linux
$ ./install.sh
```
#### Using make: before running your project do:
```
$ . <ofxDepthDevice directory>/load_ld.sh
$ cd <ofxDepthDevice directory>/example-empty
$ make && make run
 ```
 if you want to make this permanent, you could add `. <ofxDepthDevice directory>/load_ld.sh` to your .bashrc

#### Using qtCreator:
```
$ . <ofxDepthDevice directory>/load_ld.sh
$ echo $LD_LIBRARY_PATH
<ofxDepthDevice directory>/libs/libk4abt/lib/linux64/:<ofxDepthDevice directory>/libs/libk4a/lib/linux64/
 ```
 copy the output and add it as environment variable LD_LIBRARY_PATH in build environment

 Ideally this would be done by the addon, and the shared libraries somehow automatically linked or added to LD_LIBRARY_PATH.

#### increase usb memory

* make sure you have enough usb memory. check your memory with `cat /sys/module/usbcore/parameters/usbfs_memory_mb`
* this is a good [resource](https://www.flir.com/support-center/iis/machine-vision/application-note/using-linux-with-usb-3.1/)
* TL;DR you can add `usbcore.usbfs_memory_mb=256` in your grub after `quiet splash` to get 256 MB 

### Windows (use at own risk, not tested)

* Install the [Azure Kinect Sensor SDK](https://docs.microsoft.com/en-us/azure/Kinect-dk/sensor-sdk-download).
* Install the [Azure Kinect Body Tracking SDK](https://docs.microsoft.com/en-us/azure/Kinect-dk/body-sdk-download).
* Add an environment variable for `AZUREKINECT_SDK` and set it to the Sensor SDK installation path (no trailing slash). The default is `C:\Program Files\Azure Kinect SDK v1.3.0`.
* Add an environment variable for `AZUREKINECT_BODY_SDK` and set it to the Body SDK installation path (no trailing slash). The default is `C:\Program Files\Azure Kinect Body Tracking SDK`.


* Add the path to the Sensor SDK `bin` folder to the `PATH` variable. The default is `%AZUREKINECT_SDK%\sdk\windows-desktop\amd64\release\bin`.
* Add the path to the Body SDK `bin` folder to the `PATH` variable. The default is `%AZUREKINECT_BODY_SDK%\sdk\windows-desktop\amd64\release\bin`.
* Add the path to the Body SDK `tools` folder to the `PATH` variable. The default is `%AZUREKINECT_BODY_SDK%\tools`.


* Clone this repository in your openFrameworks `addons` folder.
* You can then use the OF Project Generator to generate projects with the appropriate headers and libraries included. ✌️
* Note that if you want to use body tracking, you will need to copy the cuDNN model file `dnn_model_2_0.onnx` from the Body SDK `tools` folder into your project's `bin` folder!

## Compatibility

Tested with: 
* openFrameworks 0.10.x / 0.11.x
* Ubuntu 19.10, Qt Creator
* Ubuntu 18.04, Qt Creator 4.11.2, Qt Creator 4.12.0, Makefile
* Ubuntu 20.04, Makefile
* Debian 10, Makefile
* Debian 11, Makefile
* Manjaro 20.0, Qt Creator 4.11.2, Qt Creator 4.12.0, Makefile

## Examples

* `example-multi-pointcloud` demonstrates how to stream a point cloud from multiple devices individually.
* `example-devicePool` demonstrates how to stream a point cloud from multiple devices using a device pool.
* `example-save-model` demonstrates how to save a point cloud from a device as *.obj or *.ply.
* `example-raw-*` demonstrate how to interface directly with Kinect Azure SDK.

## Structure

The structure of this addon is in progress.
A rough sketch of a possible calibration method, and a specific two-computer application of a "transporter" (scanner) and "simulation" (rendering):
  ![structure](./structure.svg)

## Profiling
Profiling 6 KinectMT in release mode (debug is slower and has more gaps)
Each camera has three threads (update frameset, update pointcloud, update mesh(this is actually one step too much)).
last 4 lines are the main thread
  ![profiling](./profiling.png)

## Thanks

This addon was largely inspired by the simplicity of UnaNancyOwen's [KinectAzureExample](https://github.com/UnaNancyOwen/AzureKinectSample), prisonerjohn's (Elie Zananiri) great work on the [ofxAzureKinect addon](https://github.com/prisonerjohn/ofxAzureKinect) and also from ofTheo's (Theodore Watson) [ofxKinectV2](https://github.com/ofTheo/ofxKinectV2).
