#!/bin/bash

ldmunge () {
  if ! echo "$LD_LIBRARY_PATH" | /bin/grep -Eq "(^|:)$1($|:)" ; then
     if [ "$2" = "after" ] ; then
        export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$1"
     else
        export LD_LIBRARY_PATH="$1:$LD_LIBRARY_PATH"
     fi
  fi
}

CURRENT_DIR=$(pwd)
OFX_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
cd ../..
SCRIPT_OF_ROOT=$(pwd)
cd $CURRENT_DIR

ldmunge ${OFX_DIR}/libs/libk4abt/lib/linux64/
ldmunge ${OFX_DIR}/libs/libk4a/lib/linux64/
ldmunge ${OFX_DIR}/libs/turbojpeg/lib/linux64/

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
