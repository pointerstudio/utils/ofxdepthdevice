#pragma once

#include "ofMain.h"
#include "ofxDepthDevice.h"
#include "ofxLaunchControllers.h"
#include "ofxGui.h"

using namespace ofxDepthDevice;

struct AppParameters {
    ofParameter <bool> updateDevices;
    ofParameter <bool> drawDevices;
    ofParameter <bool> drawDebug;
    ofParameter <bool> saveModel;
    ofParameter <glm::vec3> center;
    ofParameter <glm::vec3> orientation;
};

class DeviceParameters {
    public:
        ofParameter <bool> draw_device;
        ofParameter <bool> draw_debug;
        ofParameter <int> position_index;
        ofParameter <glm::vec3> center;
        ofParameter <glm::vec3> centerFine;
        ofParameter <glm::vec3> position;
        ofParameter <glm::vec3> positionFine;
        ofParameter <glm::vec3> orientation;
        ofParameter <glm::vec3> orientationFine;
        ofParameter <glm::vec3> cropPosition;
        ofParameter <float> cropHeight;
        ofParameter <float> cropRadius;
        void setDeviceExtrinsics(shared_ptr <DepthDevice> & device, const AppParameters & appParameters);
        void setDeviceCropping(shared_ptr <DepthDevice> & device, const AppParameters & appParameters);
        void drawDebug(shared_ptr <DepthDevice> device);
};


class ofApp : public ofBaseApp {

    public:
        void setup();
        void update();
        void draw();
        void exit();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        void saveModel();
        void saveModelPly();
        void saveModelObj();

        void lcListener();
        ofParameter <int> lc_enabledDevice;

        void changedPosition(glm::vec3 & e);
        void deviceParametersListener(ofAbstractParameter & e);
        void appParametersListener(ofAbstractParameter & e);

        AppParameters appParameters;

        vector <shared_ptr <DepthDevice> > kirnects;
//        ofxLaunchControl lc;
        ofxPanel gui;
        vector <DeviceParameters> deviceParameters;

        ofEasyCam cam;

        std::thread saviour;
        std::atomic <bool> is_saviour_saving;
        std::atomic <bool> is_saviour_ready;

};
