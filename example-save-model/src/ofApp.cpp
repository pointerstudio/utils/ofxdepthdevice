#include "ofApp.h"

#include <glm/gtx/matrix_decompose.hpp>
//--------------------------------------------------------------
void ofApp::setup(){

    gui.setup("", "settings.json", 20, 20);
    ofJson guiSettings = ofLoadJson("settings.json");
    string profilerJson = ofGetTimestampString() + "_result.json";
    OFX_PROFILER_BEGIN_SESSION("kirnects", profilerJson);

    ofParameterGroup appGroup;
    appGroup.setName("app_parameters");
    appGroup.add(appParameters.updateDevices.set("update_devices", true));
    appGroup.add(appParameters.drawDevices.set("draw_devices", true));
    appGroup.add(appParameters.drawDebug.set("draw_debug", true));
    appGroup.add(appParameters.saveModel.set("save_model", false));
    appGroup.add(appParameters.center.set("center", glm::vec3(0), glm::vec3(-500), glm::vec3(500)));
    appGroup.add(appParameters.orientation.set("orientation", glm::vec3(0), glm::vec3(-180), glm::vec3(180)));
    ofAddListener(appGroup.parameterChangedE(), this, &ofApp::appParametersListener);
    gui.add(appGroup);
    const int32_t device_count = k4a::device::get_installed_count();
    ofParameterGroup groups[device_count];
    for(int i = 0; i < device_count; i++){
        // setup kinects
        auto kirnect = make_shared <Kinect>();
        kirnect->setup(i);
        string serial_number = kirnect->getSerialNumber();
        cout << kirnect->getSerialNumber();
        stringstream ss_name;
        ss_name << "device_" << ofToString(i);


        kirnect->device_index = i;
        cout << "setup kinect " << i << endl;
        kirnects.push_back(kirnect);

        // setup gui
        ofParameterGroup group;
        DeviceParameters dp;

        int position_index = i;

        ofJson o = guiSettings["group"];
        if(o.find(ss_name.str()) != o.end()){
            ofJson s = o[ss_name.str()];
            if(s.find("position_index") != s.end()){
                position_index = ofToInt(s["position_index"].get <string>());
            }
        }

        group.setName(ss_name.str());
        group.add(dp.draw_device.set("draw_device", true));
        group.add(dp.draw_debug.set("draw_debug", true));
        group.add(dp.position_index.set("position_index", position_index, 0, device_count));
        group.add(dp.center.set("center", glm::vec3(0), glm::vec3(-500), glm::vec3(500)));
        group.add(dp.centerFine.set("centerFine", glm::vec3(0), glm::vec3(-10), glm::vec3(10)));
        group.add(dp.position.set("position", glm::vec3(0), glm::vec3(-500), glm::vec3(500)));
        group.add(dp.positionFine.set("positionFine", glm::vec3(0), glm::vec3(-10), glm::vec3(10)));
        group.add(dp.orientation.set("orientation", glm::vec3(0), glm::vec3(-360), glm::vec3(360)));
        group.add(dp.orientationFine.set("orientationFine", glm::vec3(0), glm::vec3(-45), glm::vec3(45)));
        group.add(dp.cropPosition.set("cropPosition", glm::vec3(0, 0, 120), glm::vec3(-300), glm::vec3(300)));
        group.add(dp.cropRadius.set("cropRadius", 70, 1, 200));
        group.add(dp.cropHeight.set("cropHeight", 200, 1, 300));

        ofAddListener(group.parameterChangedE(), this, &ofApp::deviceParametersListener);
        groups[position_index] = group;
        gui.add(group);
        gui.getGroup(ss_name.str()).minimizeAll();
        gui.getGroup(ss_name.str()).minimize();
        deviceParameters.push_back(dp);
    }

    gui.loadFromFile("settings.json");

    cam.setPosition(200, 200, 200);
    cam.lookAt(glm::vec3(0, 0, 0));
//    lc_enabledDevice.set(0);
//    lcListener();
//    lc_enabledDevice.newListener([&](int &){
//        return this->lcListener();
//    });

    is_saviour_saving.store(false);
    is_saviour_ready.store(false);
}

void ofApp::lcListener(){
//    int device_index = lc_enabledDevice.get();
//    cout << "enable device " << ofToString(device_index) << endl;
//    auto & dp = deviceParameters[device_index];
//    lc.clear();
//    lc.setup();
//    lc.radio(0, 5, lc_enabledDevice, ofxLCLeds::Red, ofxLCLeds::Green);
//    lc.toggle(6, dp.draw_device);
//    lc.toggle(7, dp.draw_debug);
//    lc.knob3(0, dp.center, dp.center.getMin(), dp.center.getMax());
//    lc.knob3(3, dp.position, dp.position.getMin(), dp.position.getMax());
//    lc.knob3(6, dp.orientation, dp.orientation.getMin(), dp.orientation.getMax());
//    lc.knob3(4, dp.position, dp.position.getMin(), dp.position.getMax());
}

void ofApp::appParametersListener(ofAbstractParameter & e){
    cout << "app parameter listener: " << e.getName() << " lol" << endl;
    if(e.getName() == "update_devices"){
        bool v = e.cast <bool>();
        for(auto & k : kirnects){
            k->setUpdating(v);
        }
    }
    if(e.getName() == "draw_devices"){
        bool v = e.cast <bool>();
        for(auto & p : deviceParameters){
            p.draw_device.set(v);
        }
    }
    if(e.getName() == "draw_debug"){
        bool v = e.cast <bool>();
        for(auto & p : deviceParameters){
            p.draw_debug.set(v);
        }
    }
    if(e.getName() == "save_model"){
        bool v = e.cast <bool>();
        if(v && !is_saviour_saving.load()){
            for(auto & k : kirnects){
                k->setUpdating(false);
            }
            saveModel();
        }
    }
    cout << "app parameter listener: " << e.getName() << " bums" << endl;
    const vector <string> extrinsics{"center",  "orientation"};
    cout << "app parameter listener: " << e.getName() << " schalingo" << endl;
    if(find(extrinsics.begin(), extrinsics.end(), e.getName()) != extrinsics.end()){
        cout << "app parameter listener: " << e.getName() << " yes it is" << endl;
        for(int i = 0; i < deviceParameters.size(); i++){
            cout << "app parameter listener: " << e.getName() << " device " << i << endl;
            deviceParameters[i].setDeviceExtrinsics(kirnects[i], appParameters);
            deviceParameters[i].setDeviceCropping(kirnects[i], appParameters);
        }
    }
    cout << "app parameter listener: " << e.getName() << endl;
}

void ofApp::deviceParametersListener(ofAbstractParameter & e){
    int hierarchy_device = 1;
    string deviceString = e.getGroupHierarchyNames()[hierarchy_device];
    string deviceIndexString = deviceString;
    ofStringReplace(deviceIndexString, "device_", "");
    int deviceIndex = ofToInt(deviceIndexString);

    const vector <string> extrinsics{"center", "position", "orientation", "centerFine", "positionFine", "orientationFine"};
    if(find(extrinsics.begin(), extrinsics.end(), e.getName()) != extrinsics.end()){
        deviceParameters[deviceIndex].setDeviceExtrinsics(kirnects[deviceIndex], appParameters);
    }
    const vector <string> localCropping{"cropPosition", "cropHeight", "cropRadius"};
    if(find(localCropping.begin(), localCropping.end(), e.getName()) != localCropping.end()){
        deviceParameters[deviceIndex].setDeviceCropping(kirnects[deviceIndex], appParameters);
    }
}

void DeviceParameters::setDeviceExtrinsics(shared_ptr <DepthDevice> & device, const AppParameters & appParameters){
    ofNode & e = device->extrinsics;
//    e.setPosition(0, 0, 0);
//    e.setOrientation(glm::quat());
    e.resetTransform();

    glm::mat4 transformation; // your transformation matrix.

    transformation = glm::translate(transformation, center.get());
    transformation = glm::translate(transformation, centerFine.get());

//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().x, glm::vec3(1, 0, 0));
//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().y, glm::vec3(0, 1, 0));
//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().z, glm::vec3(0, 0, 1));

    transformation = glm::rotate(transformation, glm::radians(orientation.get().x), glm::vec3(1, 0, 0));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().x), glm::vec3(1, 0, 0));
    transformation = glm::rotate(transformation, glm::radians(orientation.get().y), glm::vec3(0, 1, 0));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().y), glm::vec3(0, 1, 0));
    transformation = glm::rotate(transformation, glm::radians(orientation.get().z), glm::vec3(0, 0, 1));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().z), glm::vec3(0, 0, 1));

    transformation = glm::translate(transformation, -center.get());
    transformation = glm::translate(transformation, -centerFine.get());
    transformation = glm::translate(transformation, position.get());
    transformation = glm::translate(transformation, positionFine.get());

    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(transformation, scale, rotation, translation, skew, perspective);
//    e.setPosition(translation);
    glm::vec3 newPosition = translation + appParameters.center.get();
    // TODO: implement rotation
//    glm::quat newRotation = glm::rotate(rotation, glm::radians(appParameters.orientation.get()));
    e.setPosition(newPosition);
    e.setOrientation(rotation);
//    device->applyExtrinsics();

    cout << "set device extrinsics " << ofToString(device->device_index) << endl;
}
void DeviceParameters::setDeviceCropping(shared_ptr <DepthDevice> & device, const AppParameters & appParameters){
    device->localCropping.resetTransform();
    device->localCropping.setResolutionHeight(1);
    device->localCropping.setResolutionRadius(16);
    device->localCropping.setHeight(cropHeight.get());
    device->localCropping.setRadius(cropRadius.get());
    device->localCropping.setPosition(cropPosition.get() + appParameters.center.get());
    device->localCropping.rotateDeg(90, 0, 0, 1);
    device->localCropping.rotate(appParameters.orientation.get());
    cout << "set device cropping " << ofToString(device->device_index) << endl;
}

void DeviceParameters::drawDebug(shared_ptr <DepthDevice> device){
    ofPushStyle();
    ofSetColor(ofColor::lightBlue);
    ofDrawBox(center.get(), 2);
    ofSetColor(ofColor::lightPink);
    ofDrawBox(position.get(), 2);
    ofSetColor(ofColor::lightGreen);
    device->localCropping.drawWireframe();
    device->extrinsics.transformGL();
    ofDrawAxis(300);
    device->extrinsics.restoreTransformGL();
    ofPopStyle();
}

void ofApp::changedPosition(glm::vec3 & e){
    cout << e.x << endl;
    kirnects[0]->extrinsics.setPosition(e);
//do something
}

//--------------------------------------------------------------
void ofApp::update(){
    OFX_PROFILER_FUNCTION();
    for(auto & kirnect : kirnects){
        kirnect->update();
        kirnect->draw();
    }
    if(is_saviour_ready.load()){
        if(saviour.joinable()){
            saviour.join();
            is_saviour_saving.store(false);
            is_saviour_ready.store(false);
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    OFX_PROFILER_FUNCTION();
    ofBackground(is_saviour_saving.load() ? ofColor::red : ofColor::darkGray);
    ofEnableDepthTest();
    int i = 0;
    for(auto & kirnect : kirnects){
        auto & p = deviceParameters[i];
        cam.begin();
        if(p.draw_device.get()){
            kirnect->show();
            ofDrawAxis(500);
        }
        if(p.draw_debug.get()){
            p.drawDebug(kirnect);
        }
        cam.end();
        i++;
    }
    ofDisableDepthTest();
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::exit(){
    OFX_PROFILER_END_SESSION();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if(key == 'u'){
        for(auto & kirnect : kirnects){
            kirnect->toggleUpdating();
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void ofApp::saveModel(){
    appParameters.updateDevices.set(false);
    saviour = std::thread([&] {
        is_saviour_saving.store(true);
        std::this_thread::sleep_for(1s);
        saveModelObj();
        appParameters.saveModel.set(false);
        appParameters.updateDevices.set(true);
        is_saviour_ready.store(true);
    });
}

void ofApp::saveModelPly(){
    ofVboMesh mesh;
    for(auto & k : kirnects){
        mesh.addVertices(k->getVboMesh()->getVertices());
        mesh.addColors(k->getVboMesh()->getColors());
        mesh.addTexCoords(k->getVboMesh()->getTexCoords());
    }
    string timestamp = ofGetTimestampString("%Y.%m.%d_%H:%M:%S");
    mesh.save("mesh_" + timestamp + ".ply");
}

void ofApp::saveModelObj(){
    string timestamp = ofGetTimestampString("%Y.%m.%d_%H:%M:%S");
    string dirString = "models/" + timestamp;

    ofDirectory dir(dirString);
    dir.create(true);

    ofFile obj;
    obj.open(dirString + "/model.obj", ofFile::WriteOnly);
    obj << std::fixed << "# the combined points\n\n";
    obj << std::fixed << "mtllib material.mtl\n";
    obj << std::fixed << "o human\n\n";

    ofFile mtl;
    mtl.open(dirString + "/material.mtl", ofFile::WriteOnly);
    for(auto & k : kirnects){
        string color_image_name = k->getSerialNumber() + "_tex_Color.png";
        string color_image_path = dirString + "/" + color_image_name;
        mtl << std::fixed
            << "newmtl mtl_" << k->getSerialNumber() << "\n"
            << "Ka 1.000 1.000 1.000\n"
            << "Kd 1.000 1.000 1.000\n"
            << "Ks 0.000 0.000 0.000\n"
            << "d 1.0\n"
            << "illum 2\n"
            << "map_Ka " << color_image_name << "\n"
            << "map_Kd " << color_image_name << "\n"
            << "\n\n" << std::endl;

        obj << std::fixed << "usemtl mtl_" << k->getSerialNumber() << "\n"
            << "g group" << k->getSerialNumber() << "\n";

        vector <glm::vec3> vertices = k->getVboMesh()->getVertices();
        vector <ofFloatColor> colors = k->getVboMesh()->getColors();
        vector <glm::vec2> texCoords = k->getVboMesh()->getTexCoords();

        for(int i = 0; i < vertices.size(); i++){
            obj << "v " << std::fixed << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << " " << colors[i].r << " " << colors[i].g << " " << colors[i].b << std::endl;
            obj << "vt " << std::fixed << texCoords[i].x << " " << texCoords[i].y << std::endl;
        }

        k->getColorImage()->save(color_image_path);
    }
}
