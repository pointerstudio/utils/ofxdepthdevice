#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main(){
    ofGLWindowSettings settings;
    settings.setGLVersion(3, 3);
    settings.setSize(1920, 1080);
    settings.windowMode = OF_FULLSCREEN;
    ofCreateWindow(settings);           // <-------- setup the GL context
    ofRunApp(new ofApp());

}
