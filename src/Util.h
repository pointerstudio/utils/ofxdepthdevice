#pragma once

/*
 This is utility to that provides converter to convert k4a::image to cv::Mat.

 cv::Mat mat = k4a::get_mat( image );

 Copyright (c) 2019 Tsukasa Sugiura <t.sugiura0204@gmail.com>
 Licensed under the MIT license.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/

#include <vector>
#include <limits>

#include <k4a/k4a.h>
#include <k4a/k4a.hpp>
#include <opencv2/opencv.hpp>
#include <turbojpeg.h>
#include "ofMain.h"
#include "ofxCv.h"
#include "PreprocessorConfig.h"
#include <glm/gtx/matrix_decompose.hpp>

namespace k4a {
cv::Mat get_mat(k4a::image & src, bool deep_copy = true);
void toOf(const k4a::image & src, ofImage & target);
//void toOf(k4a::image & colorSrc, k4a::image & depthSrc, ofVboMesh & target);
static bool xyz_color_to_depth(k4a_transformation_t transformation_handle,
                               const k4a_image_t depth_image,
                               const k4a_image_t color_image,
                               k4a_image_t & transformed_color_image,
                               k4a_image_t & xyz_image);

static bool xyz_depth_to_color(k4a_transformation_t transformation_handle,
                               const k4a_image_t depth_image,
                               const k4a_image_t color_image,
                               k4a_image_t & xyz_image);

//bool toOf(k4a::image xyz_image,
//          k4a::image color_image,
//          glm::vec3 & depthUnitScale,
//          ofNode & extrinsics,
//          ofVboMesh & target_pointcloud);

bool toOf(k4a::image xyz_image,
          k4a::image color_image,
          glm::vec3 & depthUnitScale,
          glm::mat4 & extrinsics,
          ofCylinderPrimitive & localCropping,
          ofVboMesh & target_pointcloud);

//bool toOf(const k4a::image & xyz_image,
//          const k4a::image & color_image,
//          const glm::vec3 & depthUnitScale,
//          const ofNode & extrinsics,
//          const ofCylinderPrimitive & localCropping,
//          ofVboMesh & target_pointcloud);

bool intrinsicsToCv(const k4a_calibration_intrinsic_parameters_t & intrinsics,
                    cv::Mat & cameraMatrix,
                    cv::Mat & distCoeffs);
} // end namespace k4a

namespace ofxDepthDevice {
ofImage k4a_get_ofImage(k4a::image & src, bool deep_copy = true);
cv::Mat k4a_get_mat(k4a_image_t & src, bool deep_copy = true);

static const k4a_device_configuration_t K4A_DEVICE_CONFIG_OFXK4AMINI_DEFAULT = {K4A_IMAGE_FORMAT_COLOR_BGRA32,
                                                                                K4A_COLOR_RESOLUTION_1440P,
                                                                                K4A_DEPTH_MODE_NFOV_UNBINNED,
                                                                                K4A_FRAMES_PER_SECOND_15,
                                                                                true,
                                                                                0,
                                                                                K4A_WIRED_SYNC_MODE_STANDALONE,
                                                                                0,
                                                                                false};

static const k4a_device_configuration_t K4A_DEVICE_CONFIG_OFXK4AMINI_WIDE = {K4A_IMAGE_FORMAT_COLOR_BGRA32,
                                                                             K4A_COLOR_RESOLUTION_1440P,
                                                                             K4A_DEPTH_MODE_NFOV_UNBINNED,
                                                                             K4A_FRAMES_PER_SECOND_30,
                                                                             true,
                                                                             0,
                                                                             K4A_WIRED_SYNC_MODE_STANDALONE,
                                                                             0,
                                                                             false};

}
