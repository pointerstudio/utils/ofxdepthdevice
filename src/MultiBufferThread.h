#pragma once

#include <thread>
#include <atomic>
#include <functional>
#include "PreprocessorConfig.h"

template <class T, int I>
class MultiBufferThread {
    public:
        int n_buffers;
        std::thread worker;
        std::atomic <bool> is_thread_running;
        std::atomic <bool> is_requested;
        std::atomic <int> FRESH_INDEX;
        std::atomic <int> WRITE_INDEX;
        std::atomic <int> READ_INDICES[I];
        T buffers[I + 2];

        void setup(){
            is_thread_running.store(true);
            is_fresh.store(false);
//            is_requested.store(false);
            FRESH_INDEX.store(-1);
            WRITE_INDEX.store(0);

            n_buffers = I + 2;
            for(int i = 0; i < I; i++){
                READ_INDICES[i].store(-1);
            }
        }
    private:
        std::atomic <bool> is_fresh;
        int getNextWriteIndex(){
            OFX_PROFILER_FUNCTION();
            int stale = FRESH_INDEX.load();
            int write_candidate = 0;
            int read_indices[I];
            for(int i = 0; i < I; i++){
                read_indices[i] = READ_INDICES[i].load();
            }

            for(int i = 0; i < 5; i++){
                if(i == stale){
                    continue;
                }else{
                    bool skipped = false;
                    for(int j = 0; j < 3; j++){
                        if(i == read_indices[j]){
                            skipped = true;
                            continue;
                        }
                    }
                    if(!skipped){
                        write_candidate = i;
                        break;
                    }
                }
            }
            return write_candidate;
        }
        int getFreshReadIndex(int reader){
            is_fresh.store(false);
            int fresh = FRESH_INDEX.load();
            int current = READ_INDICES[reader].load();
            if(fresh == current){
                return current;
            }
            READ_INDICES[reader].store(fresh);
            return fresh;
        }
    public:
        bool isFresh(int reader = -1){
            if(FRESH_INDEX.load() == -1){
                return false;
            }
            if(reader == -1){
                return is_fresh.load();
            }
            if(is_fresh.load()){
                return true;
            }
            return FRESH_INDEX.load() != READ_INDICES[reader].load();
        }
        void setFresh(){
            FRESH_INDEX.store(WRITE_INDEX.load());
            is_fresh.store(true);
        }
        const T & getCurrentReadBuffer(int reader){
            return buffers[READ_INDICES[reader]];
        }
        const T & getFreshReadBuffer(int reader){
            return buffers[getFreshReadIndex(reader)];
        }
        T & getCurrentWriteBuffer(){
            return buffers[WRITE_INDEX.load()];
        }
        T & getNextWriteBuffer(){
            WRITE_INDEX.store(getNextWriteIndex());
            return buffers[WRITE_INDEX.load()];
        }
        void close(){
            is_thread_running.store(false);
            worker.join();
        }
};

class SubThread {
    public:
        std::atomic <bool> has_work;
        void close(){
            is_thread_running.store(false);
            worker.join();
        }
        void setup(){
            is_thread_running.store(true);
            has_work.store(false);
        }
        std::thread worker;
        std::atomic <bool> is_thread_running;
};
