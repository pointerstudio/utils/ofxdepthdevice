#include "DepthDevice.h"

namespace ofxDepthDevice {
DepthDevice::DepthDevice(){
    depthDeviceType = OFXDD_UNKNOWN_DEVICE;
    depthUnitScale = glm::vec3(1);
    calibrationExtrinsics = glm::mat4(1); // identity matrix
    localCropping.setHeight(0);
    localCropping.setRadius(0);
}
DepthDevice::~DepthDevice(){
    if(is_initialized.load()){
        close();
    }
}
void DepthDevice::close(){
    is_initialized.store(false);
}
bool DepthDevice::readIntrinsics(cv::Mat & cameraMatrix, cv::Mat & distCoeffs){
    return false;
}
glm::mat4 DepthDevice::getColorToDepth(){
    return glm::mat4();
}
string DepthDevice::getSerialNumber(){
    return serial_number;
}
void DepthDevice::toggleUpdating(){
    is_updating.store(!is_updating.load());
}
void DepthDevice::setUpdating(bool _is_updating){
    is_updating.store(_is_updating);
}

//void DepthDevice::DeviceParameters::setDeviceExtrinsics(shared_ptr <ofxDepthDevice::DepthDevice> & device){
//    ofNode & e = device->manualExtrinsics;
////    e.setPosition(0, 0, 0);
////    e.setOrientation(glm::quat());
//    e.resetTransform();

//    glm::mat4 transformation; // your transformation matrix.

//    transformation = glm::translate(transformation, center.get());
//    transformation = glm::translate(transformation, centerFine.get());

////    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().x, glm::vec3(1, 0, 0));
////    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().y, glm::vec3(0, 1, 0));
////    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().z, glm::vec3(0, 0, 1));

//    transformation = glm::rotate(transformation, glm::radians(orientation.get().x), glm::vec3(1, 0, 0));
//    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().x), glm::vec3(1, 0, 0));
//    transformation = glm::rotate(transformation, glm::radians(orientation.get().y), glm::vec3(0, 1, 0));
//    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().y), glm::vec3(0, 1, 0));
//    transformation = glm::rotate(transformation, glm::radians(orientation.get().z), glm::vec3(0, 0, 1));
//    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().z), glm::vec3(0, 0, 1));

//    transformation = glm::translate(transformation, -center.get());
//    transformation = glm::translate(transformation, -centerFine.get());
//    transformation = glm::translate(transformation, position.get());
//    transformation = glm::translate(transformation, positionFine.get());

//    glm::vec3 scale;
//    glm::quat rotation;
//    glm::vec3 translation;
//    glm::vec3 skew;
//    glm::vec4 perspective;
//    glm::decompose(transformation, scale, rotation, translation, skew, perspective);
////    e.setPosition(translation);
//    glm::vec3 newPosition = translation;
//    // TODO: implement rotation
////    glm::quat newRotation = glm::rotate(rotation, glm::radians(appParameters.orientation.get()));
//    e.setPosition(newPosition);
//    e.setOrientation(rotation);
////    device->applyExtrinsics();

////    cout << "set device extrinsics " << ofToString(device->device_index) << endl;
//}
//void DepthDevice::DeviceParameters::setDeviceCropping(shared_ptr <ofxDepthDevice::DepthDevice> & device){
//    device->localCropping.resetTransform();
//    device->localCropping.setResolutionHeight(1);
//    device->localCropping.setResolutionRadius(16);
//    device->localCropping.setHeight(cropHeight.get());
//    device->localCropping.setRadius(cropRadius.get());
//    device->localCropping.setPosition(cropPosition.get());
//    device->localCropping.rotateDeg(90, 0, 0, 1);
////    cout << "set device cropping " << ofToString(device->device_index) << endl;
//}

void DepthDevice::setCalibrationExtrinsics(const glm::mat4 transformationMatrix){
    calibration_mtx.lock();
    calibrationExtrinsics = transformationMatrix;
    calibration_mtx.unlock();
}

glm::mat4 DepthDevice::getCalibrationExtrinsics(){
    // we cannot return a reference to the calibrationExtrinsics,
    // without breaking the lock
    calibration_mtx.lock();
    glm::mat4 out(calibrationExtrinsics);
    calibration_mtx.unlock();
    return out;
}

glm::mat4 DepthDevice::getExtrinsics(){
    calibration_mtx.lock();
    glm::mat4 extrinsics = refinementExtrinsics * manualExtrinsics.getGlobalTransformMatrix() * calibrationExtrinsics;
    calibration_mtx.unlock();
    return extrinsics;
}

void DepthDevice::setRefinementExtrinsics(const glm::mat4 transformationMatrix){
    calibration_mtx.lock();
    refinementExtrinsics = transformationMatrix * refinementExtrinsics;
    calibration_mtx.unlock();
}

glm::mat4 DepthDevice::getRefinementExtrinsics(){
    // we cannot return a reference to the refinementExtrinsics,
    // without breaking the lock
    calibration_mtx.lock();
    glm::mat4 out(refinementExtrinsics);
    calibration_mtx.unlock();
    return out;
}

void DepthDevice::setLocalCropping(const ofCylinderPrimitive cropping){
    calibration_mtx.lock();
    localCropping = cropping;
    calibration_mtx.unlock();
}

ofCylinderPrimitive DepthDevice::getLocalCropping(){
    calibration_mtx.lock();
    ofCylinderPrimitive cropping(localCropping);
    calibration_mtx.unlock();
    return cropping;
}

//void DepthDevice::DeviceParameters::drawDebug(shared_ptr <ofxDepthDevice::DepthDevice> device){
//    ofPushStyle();
//    ofSetColor(ofColor::lightBlue);
//    ofDrawBox(center.get(), 2);
//    ofSetColor(ofColor::lightPink);
//    ofDrawBox(position.get(), 2);
//    ofSetColor(ofColor::lightGreen);
//    device->localCropping.drawWireframe();
//    device->manualExtrinsics.transformGL();
//    ofDrawAxis(300);
//    device->manualExtrinsics.restoreTransformGL();
//    ofPopStyle();
//}
}
