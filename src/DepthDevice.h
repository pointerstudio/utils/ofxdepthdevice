#pragma once

#include "Util.h"
#include "CalibrationHelper/CalibrationHelper.h"

namespace ofxDepthDevice {

/*
 * the device class
 */
class DepthDevice {
    private:
    public:

        enum DepthDeviceType {
            OFXDD_UNKNOWN_DEVICE, OFXDD_K4A, OFXDD_RealSense
        };
//        class DeviceParameters {
//            public:
//                ofParameter <bool> draw_device;
//                ofParameter <bool> draw_debug;
//                ofParameter <int> position_index;
//                ofParameter <glm::vec3> center;
//                ofParameter <glm::vec3> centerFine;
//                ofParameter <glm::vec3> position;
//                ofParameter <glm::vec3> positionFine;
//                ofParameter <glm::vec3> orientation;
//                ofParameter <glm::vec3> orientationFine;
//                ofParameter <glm::vec3> cropPosition;
//                ofParameter <float> cropHeight;
//                ofParameter <float> cropRadius;
//                void setDeviceExtrinsics(shared_ptr <DepthDevice> & device);
//                void setDeviceCropping(shared_ptr <DepthDevice> & device);
//                void drawDebug(shared_ptr <DepthDevice> device);
//        };

        DepthDevice();
        virtual ~DepthDevice();
        virtual void setup() = 0;
        virtual void update() = 0;
        virtual void draw(bool fresh = true) = 0;
        virtual void close();
        virtual bool readIntrinsics(cv::Mat & cameraMatrix, cv::Mat & distCoeffs);
        virtual glm::mat4 getColorToDepth();

        string getSerialNumber();
        DepthDeviceType depthDeviceType;
//        CalibrationHelper::Helper calibrationHelper;

        string serial_number;
        glm::vec3 depthUnitScale;
        ofNode manualExtrinsics;

        void setCalibrationExtrinsics(const glm::mat4 transformationMatrix);
        glm::mat4 getCalibrationExtrinsics();
        glm::mat4 calibrationExtrinsics;
        void setRefinementExtrinsics(const glm::mat4 transformationMatrix);
        glm::mat4 getRefinementExtrinsics();
        glm::mat4 refinementExtrinsics;
        std::mutex calibration_mtx;
        glm::mat4 getExtrinsics();

        ofNode imu;
        void setLocalCropping(const ofCylinderPrimitive cropping);
        ofCylinderPrimitive getLocalCropping();
        ofCylinderPrimitive localCropping;
        const virtual ofVboMesh & getVboMesh(bool fresh = true) = 0;
        virtual void requestScan() = 0;
        const virtual bool getScan(ofVboMesh & scan, ofImage & colorImage) = 0;
        virtual ofImage getColorImage(bool fresh = true) = 0;
        virtual ofImage getColorImageThumb(bool fresh = true) = 0;

        const virtual cv::Mat & getCalibrationColorCv(bool fresh = true) = 0;

        void toggleUpdating();
        void setUpdating(bool _is_updating);
        std::atomic <bool> is_updating;
//        std::atomic <bool> is_calibrating;

        std::atomic <bool> is_initialized;
};
}
