#ifndef CALIBRATOREXTRINSICS_H
#define CALIBRATOREXTRINSICS_H

#include "ofMain.h"
#include "ofxCv.h"
#include <opencv2/aruco/charuco.hpp>
#include "Strukturen.h"
#include "CalibrationUtils.h"

namespace CalibrationHelper {
class CalibratorExtrinsics {
    public:
        CalibratorExtrinsics();
        void setup();
        bool detectPoseOnFrame(const cv::Mat & image,
                               CommonData & commonData,
                               const bool refineStrategy = true);

        // TODO: fix inconsistent function style
        static bool detectPosesOnFrame(const cv::Mat & image,
                                       const cv::Mat intrinsicsCameraMatrix,
                                       const cv::Mat intrinsicsDistCoeffs,
                                       const vector <cv::Ptr <cv::aruco::CharucoBoard> > & charucoBoards,
                                       const cv::Ptr <cv::aruco::Dictionary> & dictionary,
                                       const cv::Ptr <cv::aruco::DetectorParameters> & detectorParameters,
                                       const bool refineStrategy,
                                       cv::Mat & debugImage,
                                       map <int, cv::Mat> & rvecs,
                                       map <int, cv::Mat> & tvecs);

        static bool saveToDisk(const string & fileName, const glm::mat4 & cameraPoseMatrix);
        static bool loadFromDisk(const string & fileName, glm::mat4 & cameraPoseMatrix);

};
}

#endif // CALIBRATOREXTRINSICS_H
