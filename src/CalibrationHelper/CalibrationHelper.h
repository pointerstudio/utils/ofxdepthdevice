#ifndef CALIBRATIONHELPER_H
#define CALIBRATIONHELPER_H

/*
 * STUDIO       _       _
 *  _ __   ___ (_)_ __ | |_ ___ _ ____/\__
 * | '_ \ / _ \| | '_ \| __/ _ \ '__\    /
 * | |_) | (_) | | | | | ||  __/ |  /_  _\
 * | .__/ \___/|_|_| |_|\__\___|_|    \/
 * |_|
 *       presents: calibration help
 *
 * -------------
 *
 *  License: see repository
 *
 *  studio@pointer.click
 *
 * */

#include "ofMain.h"
#include "ofxCv.h"
#include <opencv2/aruco/charuco.hpp>
#include "Strukturen.h"
#include "CalibrationUtils.h"
#include "CalibratorIntrinsics.h"
#include "CalibratorExtrinsics.h"
#include "MultiBufferThread.h"

namespace CalibrationHelper {

class Helper {
    public:
        void setup(const string id = "camera",
                   const CalibrationBoardParameters cbp = CALIBRATIONBOARD_PARAMETERS_DEFAULT);
        bool loadIntrinsicsFromDisk(string fileName = "intrinsics.yml");
        bool saveIntrinsicsToDisk(string fileName = "intrinsics.yml");
        bool loadExtrinsicsFromDisk(string fileName = "extrinsics.yml");
        bool saveExtrinsicsToDisk(string fileName = "extrinsics.yml");
        void generateBoard(bool saveImage = true);
        void intrinsicsDetectBoard(ofPixels & pixels);
        void intrinsicsCalibrateCamera(bool saveCalibration = true);
        void extrinsicsGetCameraPose(ofPixels & pixels);
        void draw(float x = 0, float y = 0);
        void drawDetectionBoards(float x = 0, float y = 0);

        CommonData commonData;
        CalibratorIntrinsics calibratorIntrinsics;
        CalibratorExtrinsics calibratorExtrinsics;
        CalibrationBoardParameters calibrationBoardParameters;
};

// for usage with multiple calibration boards and multiple cameras
class MultiHelper {
    public:

        enum CALIBRATION_READERS {
            CR_UEBER = 0,
            CR_NUM
        };

        enum UEBER_CALIBRATION_READERS {
            UCR_DRAW = 0,
            UCR_NUM
        };

        struct ExtrinsicsCalibrationIntermediate {
            cv::Mat debug_image;
            ofPixels debug_pixels_of;

            vector <int> markerIds;
            vector <vector <cv::Point2f> > markerCorners;

            map <int, cv::Mat> rvecs;
            map <int, cv::Mat> tvecs;
        };
        struct ExtrinsicsCalibrationResult {
            cv::Mat debug_image;
            ofPixels debug_pixels_of;
        };

        static void setupStatic(const vector <CalibrationBoardParameters> cbps = {CALIBRATIONBOARD_PARAMETERS_DEFAULT},
                                const string staticDirectoryPath = "calibration/static");

        static vector <cv::Ptr <cv::aruco::CharucoBoard> > sCharucoBoards;
        static vector <cv::Ptr <cv::aruco::Dictionary> > sDictionaries;
        static cv::Ptr <cv::aruco::DetectorParameters> sDetectorParameters;
        static vector <ofImage> sCharucoBoardOfImages;
        static ofDirectory sStaticDirectory;
        static MultiBufferThread <map <int, ExtrinsicsCalibrationResult>, UCR_NUM> ueberCalibrationWorker;
        static atomic <bool> sUeberCalibrating;

        // device speficic
        void setup(const string _id = "camera");
        bool getBoardPoses(const cv::Mat & mat,
                           ofImage & debugPixels);
        bool getBoardPoses(const cv::Mat & mat,
                           cv::Mat & debugImage);

        MultiBufferThread <ExtrinsicsCalibrationIntermediate, CR_NUM> calibrationWorker;

        string id;
        IntrinsicsCalibrationData intrinsics;

        ofImage extrinsics_debug_image;
        bool extrinsics_debuggable = false;
};

}

#endif // CALIBRATIONHELPER_H
