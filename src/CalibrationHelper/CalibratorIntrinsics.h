#ifndef CALIBRATORINTRINSICS_H
#define CALIBRATORINTRINSICS_H

#include "ofMain.h"
#include "ofxCv.h"
#include <opencv2/aruco/charuco.hpp>
#include "Strukturen.h"
#include "CalibrationUtils.h"

namespace CalibrationHelper {
class CalibratorIntrinsics {
    public:
        CalibratorIntrinsics();
        void setup();
        void detectBoardOnFrame(cv::Mat frame, CommonData & commonData);
        bool calibrate(CommonData & commonData);
        bool saveCameraParams(const string & filename, CommonData & commonData);
        bool loadCameraParams(const string & filename, CommonData & commonData);
        int nDetectedFrames;
        static bool loadCameraParams(const string & filename, cv::Size & imageSize, int & flags,
                                     cv::Mat & cameraMatrix, cv::Mat & distCoeffs, double & totalAvgErr);
    private:
        bool saveCameraParams(const string & filename, cv::Size imageSize, float aspectRatio, int flags,
                              const cv::Mat & cameraMatrix, const cv::Mat & distCoeffs, double totalAvgErr);

        CalibratorIntrinsicsParameters parameters;

        // collect data from each frame
        vector <vector <vector <cv::Point2f> > > allCorners;
        vector <vector <int> > allIds;
        vector <cv::Mat> allImgs;
        cv::Size imgSize;
        int calibrationFlags;
};
}
#endif // CALIBRATORINTRINSICS_H
