#include "CalibratorExtrinsics.h"
#include "PreprocessorConfig.h"

namespace CalibrationHelper {
using namespace cv;
CalibratorExtrinsics::CalibratorExtrinsics(){

}
bool CalibratorExtrinsics::detectPoseOnFrame(const cv::Mat & image,
                                             CommonData & commonData,
                                             const bool refineStrategy){
    vector <int> ids;
    vector <vector <cv::Point2f> > corners;
    vector <vector <cv::Point2f> > rejected;

    Mat image_gray;
    cvtColor(image, image_gray, COLOR_BGR2GRAY);
    // detect markers
    aruco::detectMarkers(image_gray,
                         commonData.charucoBoard->dictionary,
                         corners,
                         ids,
                         commonData.detectorParams, rejected);

    // refind strategy to detect more markers
    if(refineStrategy){
        aruco::refineDetectedMarkers(image_gray, commonData.charucoBoard, corners, ids, rejected);
    }

    // interpolate charuco corners
    Mat currentCharucoCorners, currentCharucoIds;
    if(ids.size() > 0){
        aruco::interpolateCornersCharuco(corners, ids, image_gray,
                                         commonData.charucoBoard,
                                         currentCharucoCorners,
                                         currentCharucoIds);
    }

    vector <vector <cv::Point2f> > corners_transformed;

    cout << "////////////////////////" << endl;
    cout << "id " << commonData.id << endl;
    cout << "is empty: " << (commonData.charucoBoard.empty() ? "1" : "0") << endl;
    cout << "is empty: " << (commonData.intrinsics.cameraMatrix.empty() ? "1" : "0") << endl;
    cout << "is empty: " << (commonData.intrinsics.distCoeffs.empty() ? "1" : "0") << endl;
    cout << "is empty: " << (commonData.extrinsics.boardPose.rvec.empty() ? "1" : "0") << endl;
    cout << "is empty: " << (commonData.extrinsics.boardPose.tvec.empty() ? "1" : "0") << endl;

    bool valid = aruco::estimatePoseCharucoBoard(currentCharucoCorners, currentCharucoIds,
                                                 commonData.charucoBoard,
                                                 commonData.intrinsics.cameraMatrix,
                                                 commonData.intrinsics.distCoeffs,
                                                 commonData.extrinsics.boardPose.rvec,
                                                 commonData.extrinsics.boardPose.tvec);
    if(valid){
        glm::mat4 trans = commonData.extrinsics.hasTransformation ? commonData.extrinsics.cameraTransformationMatrix : glm::mat4();
        commonData.extrinsics.cameraPoseMatrix = trans * makeCameraMatrixFromBoardPose(commonData.extrinsics.boardPose.rvec,
                                                                                       commonData.extrinsics.boardPose.tvec);

        Mat debugImage;
        cvtColor(image, debugImage, COLOR_BGRA2RGB);
        aruco::drawDetectedMarkers(debugImage, corners, noArray(), Scalar(255, 0, 0));
        if(currentCharucoCorners.total() > 4){
            aruco::drawDetectedCornersCharuco(debugImage, currentCharucoCorners, noArray(), Scalar(0, 255, 0));
            aruco::drawAxis(debugImage,
                            commonData.intrinsics.cameraMatrix,
                            commonData.intrinsics.distCoeffs,
                            commonData.extrinsics.boardPose.rvec,
                            commonData.extrinsics.boardPose.tvec,
                            50);
            ofxCv::toOf(debugImage, commonData.detectedBoardExtrinsics);
            commonData.detectedBoardExtrinsics.update();
            commonData.extrinsics.boardPose.isCalibrated = true;
            return true;
        }
    }else{
//        commonData.extrinsics.cameraPoseMatrix = glm::mat4();
        ofLogError("detectBoardPoseOnFrame") << "could not esimate charucoboard pose";
    }
    return valid;
}

bool CalibratorExtrinsics::detectPosesOnFrame(const cv::Mat & image,
                                              const cv::Mat intrinsicsCameraMatrix,
                                              const cv::Mat intrinsicsDistCoeffs,
                                              const vector <cv::Ptr <cv::aruco::CharucoBoard> > & charucoBoards,
                                              const cv::Ptr <cv::aruco::Dictionary> & dictionary,
                                              const cv::Ptr <cv::aruco::DetectorParameters> & detectorParameters,
                                              const bool refineStrategy,
                                              cv::Mat & debugImage,
                                              map <int, cv::Mat> & rvecs,
                                              map <int, cv::Mat> & tvecs){
    OFX_PROFILER_FUNCTION();
    vector <int> ids;
    vector <vector <cv::Point2f> > corners;
    vector <vector <cv::Point2f> > rejected;

    Mat image_gray;
    {
        OFX_PROFILER_SCOPE("CVT COLOR");
        cvtColor(image, image_gray, COLOR_BGR2GRAY);
    }

    {
        OFX_PROFILER_SCOPE("DETECT MARKER");
        // detect markers
        aruco::detectMarkers(image_gray,
                             dictionary,
                             corners,
                             ids,
                             detectorParameters,
                             rejected);
    }

    bool foundAnyBoard = false;

    int i = 0;
    for(auto & charucoBoard : charucoBoards){
        OFX_PROFILER_SCOPE("charuco board");
        vector <int> ids_board = ids;
        vector <vector <cv::Point2f> > corners_board = corners;
        vector <vector <cv::Point2f> > rejected_board = rejected;

        // refind strategy to detect more markers
        if(refineStrategy){
            OFX_PROFILER_SCOPE("refine");
            aruco::refineDetectedMarkers(image_gray, charucoBoard, corners_board, ids_board, rejected_board);
        }

        // interpolate charuco corners
        Mat currentCharucoCorners, currentCharucoIds;
//        vector <vector <cv::Point2f> > currentCharucoCorners, currentCharucoIds;
        if(ids.size() > 0){
            {
                OFX_PROFILER_SCOPE("interpolate");
                aruco::interpolateCornersCharuco(corners_board, ids_board, image_gray,
                                                 charucoBoard,
                                                 currentCharucoCorners,
                                                 currentCharucoIds);
            }

//            vector <vector <cv::Point2f> > corners_transformed;

            cv::Mat rvec, tvec;

            if(aruco::estimatePoseCharucoBoard(currentCharucoCorners, currentCharucoIds,
                                               charucoBoard,
                                               intrinsicsCameraMatrix,
                                               intrinsicsDistCoeffs,
                                               rvec,
                                               tvec)){
                foundAnyBoard = true;
                rvecs[i] = rvec;
                tvecs[i] = tvec;

//                aruco::drawDetectedMarkers(debugImage, corners_board, noArray(), Scalar(255, 0, 0));
                if(currentCharucoCorners.total() > 4){
                    aruco::drawDetectedCornersCharuco(debugImage, currentCharucoCorners, noArray(), Scalar(0, 255, 0));
//                    aruco::drawAxis(debugImage,
//                                    intrinsicsCameraMatrix,
//                                    intrinsicsDistCoeffs,
//                                    rvec,
//                                    tvec,
//                                    50);

                }
            }
        }
        i++;
    }
    return foundAnyBoard;
//    if(valid){
//        glm::mat4 trans = commonData.extrinsics.hasTransformation ? commonData.extrinsics.cameraTransformationMatrix : glm::mat4();
//        commonData.extrinsics.cameraPoseMatrix = trans * makeCameraMatrixFromBoardPose(commonData.extrinsics.boardPose.rvec,
//                                                                                       commonData.extrinsics.boardPose.tvec);

//        Mat debugImage;
//        cvtColor(image, debugImage, COLOR_BGRA2RGB);
//        aruco::drawDetectedMarkers(debugImage, corners, noArray(), Scalar(255, 0, 0));
//        if(currentCharucoCorners.total() > 4){
//            aruco::drawDetectedCornersCharuco(debugImage, currentCharucoCorners, noArray(), Scalar(0, 255, 0));
//            aruco::drawAxis(debugImage,
//                            commonData.intrinsics.cameraMatrix,
//                            commonData.intrinsics.distCoeffs,
//                            commonData.extrinsics.boardPose.rvec,
//                            commonData.extrinsics.boardPose.tvec,
//                            50);
//            ofxCv::toOf(debugImage, commonData.detectedBoardExtrinsics);
//            commonData.detectedBoardExtrinsics.update();
//            commonData.extrinsics.boardPose.isCalibrated = true;
//            return true;
//        }
//    }else{
////        commonData.extrinsics.cameraPoseMatrix = glm::mat4();
//        ofLogError("detectBoardPoseOnFrame") << "could not esimate charucoboard pose";
//    }
//    return valid;
}

bool CalibratorExtrinsics::saveToDisk(const string & fileName, const glm::mat4 & cameraPoseMatrix){
    cout << "cameraPoseMatrix saving to " << fileName << " as " << ofToString(cameraPoseMatrix) << endl;
    double m[16] = {0.0};

    const float * p = (const float *)glm::value_ptr(cameraPoseMatrix);
    for(int i = 0; i < 16; ++i){
        m[i] = p[i];
    }
    ofJson json;
    json["cameraPoseMatrix"] = std::vector <float>(m, m + sizeof m / sizeof m[0]);
    bool success = ofSavePrettyJson(fileName, json);
    ofLogNotice("CalibrationwExtrinsics::saveToDisk") << "save extrinsics " << fileName;
    return success;
}

bool CalibratorExtrinsics::loadFromDisk(const string & fileName, glm::mat4 & cameraPoseMatrix){
    ofFile file(fileName);
    if(file.exists()){
        ofJson json = ofLoadJson(fileName);
        vector <float> j = json["cameraPoseMatrix"].get <vector <float> >();
        cameraPoseMatrix = glm::make_mat4x4(j.data());
        ofLogNotice("CalibrationwExtrinsics::loadFromDisk") << "loaded extrinsics " << fileName;
        return true;
    }
    ofLogNotice("CalibrationwExtrinsics::loadFromDisk") << "failed loaded extrinsics " << fileName;
    return false;
}
}
