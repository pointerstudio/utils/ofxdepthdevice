#include "CalibratorIntrinsics.h"

namespace CalibrationHelper {
CalibratorIntrinsics::CalibratorIntrinsics(){
}

void CalibratorIntrinsics::setup(){
    using namespace cv;
    parameters = CALIBRATORINSTRINSICS_PARAMETERS_DEFAULT;

    calibrationFlags = 0;

    nDetectedFrames = 0;
}

void CalibratorIntrinsics::detectBoardOnFrame(cv::Mat image, CommonData & commonData){
    using namespace cv;

    vector <int> ids;
    vector <vector <Point2f> > corners, rejected;

    Mat image_gray;
    cvtColor(image, image_gray, COLOR_BGRA2GRAY);

    ofFile lol(commonData.id + "_image_gray.png");
    cv::imwrite(lol.getAbsolutePath(), image_gray);
    // detect markers
    aruco::detectMarkers(image_gray,
                         commonData.charucoBoard->dictionary,
                         corners,
                         ids,
                         commonData.detectorParams, rejected);

    // refind strategy to detect more markers
    if(parameters.refineStrategy){
        aruco::refineDetectedMarkers(image_gray, commonData.charucoBoard, corners, ids, rejected);
    }

    // interpolate charuco corners
    Mat currentCharucoCorners, currentCharucoIds;
    if(ids.size() > 0){
        aruco::interpolateCornersCharuco(corners, ids, image_gray, commonData.charucoBoard, currentCharucoCorners,
                                         currentCharucoIds);
    }

    // draw results
    if(ids.size() > 0){
        Mat debugImage;
        cvtColor(image, debugImage, COLOR_BGRA2BGR);
        aruco::drawDetectedMarkers(debugImage, corners, ids, Scalar(255, 0, 0));
        if(currentCharucoCorners.total() > 4){
            aruco::drawDetectedCornersCharuco(debugImage, currentCharucoCorners, currentCharucoIds, Scalar(0, 0, 255));
            ofxCv::toOf(debugImage, commonData.detectedBoardIntrinsics);
            commonData.detectedBoardIntrinsics.update();

//            if(allImgs.size() == 0){
//                cvtColor(image, commonData.detectedBoardPositionsIntrinsics, COLOR_BGRA2BGR);
//                commonData.detectedBoardPositionsIntrinsics.setTo(Scalar(0, 0, 0));
//                cout << "detected board positions intrinsics initialized for " << commonData.id << endl;
//            }
//            aruco::drawDetectedMarkers(commonData.detectedBoardPositionsIntrinsics, corners, ids, Scalar(255, 255, 0));
//            aruco::drawDetectedCornersCharuco(commonData.detectedBoardPositionsIntrinsics, currentCharucoCorners, currentCharucoIds, Scalar(0, 0, 255));
//            ofxCv::toOf(commonData.detectedBoardPositionsIntrinsics, commonData.ofDetectedBoardPositionsIntrinsics);
        }
        cout << commonData.id << " //////////////////////////////////////////////" << endl;
        cout << commonData.id << " //////////////////////////////////////////////" << endl;
        cout << commonData.id << " //////////////////////////////////////////////" << endl;
    }else{
        cout << commonData.id << " ______________________________________________" << endl;
        cout << commonData.id << " ______________________________________________" << endl;
        cout << commonData.id << " ______________________________________________" << endl;
        Mat debugImage;
        cvtColor(image, debugImage, COLOR_BGRA2BGR);
        ofxCv::toOf(debugImage, commonData.detectedBoardIntrinsics);
        commonData.detectedBoardIntrinsics.update();
    }

    cout << "ids size:" << ids.size() << " corners size:" << corners.size() << endl;
    cout << commonData.id << " //////////////////////////////////////////////" << endl;
    cout << commonData.id << " //////////////////////////////////////////////" << endl;
    cout << commonData.id << " //////////////////////////////////////////////" << endl;
    if(ids.size() > 0){
        if(currentCharucoCorners.total() > 4){
            cout << "Frame captured" << endl;
            if(image.channels() == 4){
                cv::Mat image_bgr;
                cvtColor(image, image_bgr, COLOR_BGRA2BGR);
                allImgs.push_back(image_bgr);
            }else{
                allImgs.push_back(image);
            }
            allCorners.push_back(corners);
            allIds.push_back(ids);
            imgSize = image.size();
            nDetectedFrames = allImgs.size();
        }
    }
}

bool CalibratorIntrinsics::saveCameraParams(const string & filename, CommonData & commonData){
    if(!commonData.intrinsics.isCalibrated){
        ofLogError("CalibratorIntrinsics::saveCameraParams") << "camera not calibrated";
        return false;
    }
    double aspectRatio = imgSize.width / (double)imgSize.height;
    return saveCameraParams(filename,
                            imgSize,
                            aspectRatio,
                            calibrationFlags,
                            commonData.intrinsics.cameraMatrix,
                            commonData.intrinsics.distCoeffs,
                            commonData.intrinsics.repError);
}

bool CalibratorIntrinsics::loadCameraParams(const string & filename, CommonData & commonData){
    return loadCameraParams(filename, imgSize, calibrationFlags,
                            commonData.intrinsics.cameraMatrix,
                            commonData.intrinsics.distCoeffs,
                            commonData.intrinsics.repError);
}

bool CalibratorIntrinsics::saveCameraParams(const string & filename, cv::Size imageSize, float aspectRatio, int flags,
                                            const cv::Mat & cameraMatrix, const cv::Mat & distCoeffs, double totalAvgErr){
    using namespace cv;
    string absoluteFilename = ofFile(filename).getAbsolutePath();
    FileStorage fs(absoluteFilename, FileStorage::WRITE);

    if(!fs.isOpened()){
        return false;
    }

    time_t tt;
    time(&tt);
    struct tm * t2 = localtime(&tt);
    char buf[1024];
    strftime(buf, sizeof(buf) - 1, "%c", t2);

    fs << "calibrationTime" << buf;

    fs << "imageSize_width" << imageSize.width;
    fs << "imageSize_height" << imageSize.height;

    if(flags & CALIB_FIX_ASPECT_RATIO){
        fs << "aspectRatio" << aspectRatio;
    }

    if(flags != 0){
        sprintf(buf, "flags: %s%s%s%s",
                flags & CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
                flags & CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
                flags & CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
                flags & CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "");
    }

    fs <<  "flags" << flags;

    fs << "cameraMatrix" << cameraMatrix;
    fs <<  "distCoeffs" << distCoeffs;

    fs <<  "reprojectionError" << totalAvgErr;

    cout << "write to " << filename << "(" << absoluteFilename << ")\n" << " cameraParams\n" << fs.releaseAndGetString() << endl;

    return true;
}

bool CalibratorIntrinsics::loadCameraParams(const string & filename, cv::Size & imageSize, int & flags,
                                            cv::Mat & cameraMatrix, cv::Mat & distCoeffs, double & totalAvgErr){
    using namespace cv;
    string absoluteFilename = ofFile(filename).getAbsolutePath();
    FileStorage fs(absoluteFilename, FileStorage::READ);

    if(!fs.isOpened()){
        return false;
    }

    int imageSize_width;
    int imageSize_height;

    fs["imageSize_width"] >> imageSize_width;
    fs["imageSize_height"] >> imageSize_height;

    imageSize.width = imageSize_width;
    imageSize.height = imageSize_height;

    fs["flags"] >> flags;

    fs["cameraMatrix"] >> cameraMatrix;
    fs["distCoeffs"] >> distCoeffs;
    fs["reprojectionError"] >> totalAvgErr;
    return true;
}

bool CalibratorIntrinsics::calibrate(CommonData & commonData){
    using namespace cv;

    int index = 0;

    cout << "captured images, moving on" << endl;

    if(allIds.size() < 1){
        cerr << "Not enough captures for calibration" << endl;
        return false;
    }

    Mat cameraMatrix, distCoeffs;
    vector <Mat> rvecs, tvecs;
    double repError;

    double aspectRatio = imgSize.width / (double)imgSize.height;
    if(calibrationFlags & CALIB_FIX_ASPECT_RATIO){
        cameraMatrix = Mat::eye(3, 3, CV_64F);
        cameraMatrix.at <double>(0, 0) = aspectRatio;
    }

    // prepare data for calibration
    vector <vector <Point2f> > allCornersConcatenated;
    vector <int> allIdsConcatenated;
    vector <int> markerCounterPerFrame;
    markerCounterPerFrame.reserve(allCorners.size());
    for(unsigned int i = 0; i < allCorners.size(); i++){
        markerCounterPerFrame.push_back((int)allCorners[i].size());
        for(unsigned int j = 0; j < allCorners[i].size(); j++){
            allCornersConcatenated.push_back(allCorners[i][j]);
            allIdsConcatenated.push_back(allIds[i][j]);
        }
    }

    cout << __LINE__ << " start aruco::calibrateCameraAruco" << endl;
    // calibrate camera using aruco markers
    double arucoRepErr;
    arucoRepErr = aruco::calibrateCameraAruco(allCornersConcatenated,
                                              allIdsConcatenated,
                                              markerCounterPerFrame, commonData.charucoBoard, imgSize, cameraMatrix,
                                              distCoeffs, noArray(), noArray(), calibrationFlags);
    cout << __LINE__ << " finished aruco::calibrateCameraAruco" << endl;

    // prepare data for charuco calibration
    int nFrames = (int)allCorners.size();
    vector <Mat> allCharucoCorners;
    vector <Mat> allCharucoIds;
    vector <Mat> filteredImages;
    allCharucoCorners.reserve(nFrames);
    allCharucoIds.reserve(nFrames);

    for(int i = 0; i < nFrames; i++){
        cout << __LINE__ << " start aruco::interpolateCornersCharuco " << i << "/" << nFrames << endl;
        // interpolate using camera parameters
        Mat currentCharucoCorners, currentCharucoIds;
        if(aruco::interpolateCornersCharuco(
               allCorners[i],
               allIds[i],
               allImgs[i],
               commonData.charucoBoard,
               currentCharucoCorners, currentCharucoIds, cameraMatrix,
               distCoeffs) == 28){
            allCharucoCorners.push_back(currentCharucoCorners);
            allCharucoIds.push_back(currentCharucoIds);
            filteredImages.push_back(allImgs[i]);
            cout << __LINE__ << " end aruco::interpolateCornersCharuco " << i << "/" << nFrames << endl;
        }
    }

    if(allCharucoCorners.size() < 4){
        cerr << "Not enough corners for calibration" << endl;
        return false;
    }

    cout << __LINE__ << " start aruco::calibrateCameraCharuco" << endl;

    // calibrate camera using charuco
    repError =
        aruco::calibrateCameraCharuco(allCharucoCorners, allCharucoIds, commonData.charucoBoard, imgSize,
                                      cameraMatrix, distCoeffs, rvecs, tvecs, calibrationFlags);
    cout << __LINE__ << " end aruco::calibrateCameraCharuco" << endl;

    commonData.intrinsics.cameraMatrix = cameraMatrix;
    commonData.intrinsics.distCoeffs = distCoeffs;
    commonData.intrinsics.rvecs = rvecs;
    commonData.intrinsics.tvecs = tvecs;
    commonData.intrinsics.repError = repError;
    commonData.intrinsics.isCalibrated = true;

    // show interpolated charuco corners for debugging
//    if(showChessboardCorners){
//        for(unsigned int frame = 0; frame < filteredImages.size(); frame++){
//            Mat imageCopy = filteredImages[frame].clone();
//            if(allIds[frame].size() > 0){

//                if(allCharucoCorners[frame].total() > 0){
//                    aruco::drawDetectedCornersCharuco(imageCopy, allCharucoCorners[frame],
//                                                      allCharucoIds[frame]);
//                }
//            }

//            imshow("out", imageCopy);
//            char key = (char)waitKey(0);
//            if(key == 27){
//                break;
//            }
//        }
//    }
    return true;
}
}
