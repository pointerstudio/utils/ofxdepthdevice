#include "CalibrationUtils.h"

namespace CalibrationHelper {

void generateCharucoBoard(const int squaresX,
                          const int squaresY,
                          const float squareLength,
                          const float markerLength,
                          const cv::Ptr <cv::aruco::Dictionary> dictionary,
                          const int boardPixelWidth,
                          const int boardPixelHeight,
                          cv::Ptr <cv::aruco::CharucoBoard> & charucoBoard,
                          cv::Ptr <cv::aruco::Board> & board,
                          cv::Mat & charucoBoardImage,
                          const int offset){
    using namespace cv;

    // camera parameters are read from somewhere
    charucoBoard = cv::aruco::CharucoBoard::create(
        squaresX,
        squaresY,
        squareLength,
        markerLength,
        dictionary);

    for(int i = 0; i < int(charucoBoard->ids.size()); i++){
        charucoBoard->ids.at(i) += offset;
    }

    cv::Size sz(boardPixelWidth, boardPixelHeight);
    cv::Mat mat;
    charucoBoard->draw(sz, mat, 0, 1);
    board = board.staticCast <aruco::Board>();

    cvtColor(mat, charucoBoardImage, COLOR_GRAY2BGRA);
}

void generateCharucoBoard(const CalibrationBoardParameters & p, CommonData & commonData){
    using namespace cv;

    cv::Ptr <cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(p.predefinedDictionaryName);

    generateCharucoBoard(p.squaresX,
                         p.squaresY,
                         p.squareLength,
                         p.markerLength,
                         dictionary,
                         p.boardPixelHeight,
                         p.boardPixelWidth,
                         commonData.charucoBoard,
                         commonData.board,
                         commonData.charucoBoardImage,
                         p.offset);

    ofImage image;
    ofxCv::toOf(commonData.charucoBoardImage, image);

    commonData.charucoBoardOfImage = image;
}

void drawCharucoBoard(const cv::Ptr <cv::aruco::CharucoBoard> & charucoBoard,
                      const int boardPixelWidth,
                      const int boardPixelHeight,
                      cv::Mat & charucoBoardImage){
    cv::Size sz(boardPixelWidth, boardPixelHeight);
    cv::Mat mat;
    charucoBoard->draw(sz, mat, 0, 1);

    cvtColor(mat, charucoBoardImage, cv::COLOR_GRAY2BGRA);

}

string getCharucoBoardFileName(const cv::Ptr <cv::aruco::CharucoBoard> & charucoBoard){
    return "charucoBoard_DICT_"
           + ofToString(charucoBoard->dictionary->markerSize) + "x"
           + ofToString(charucoBoard->dictionary->markerSize)
           + "_" + ofToString(charucoBoard->getChessboardSize().width)
           + "x" + ofToString(charucoBoard->getChessboardSize().height) + ".png";
}

glm::mat4 matrixFromRvecTvec(const cv::Mat rvec, const cv::Mat tvec){
    cv::Mat rot3x3;
    if(rvec.rows == 3 && rvec.cols == 3){
        rot3x3 = rvec;
    }else{
        Rodrigues(rvec, rot3x3);
    }
    const double * rm = rot3x3.ptr <double>(0);
    const double * tm = tvec.ptr <double>(0);
    auto m = glm::mat4(rm[0], rm[3], rm[6], 0.0f,
                       rm[1], rm[4], rm[7], 0.0f,
                       rm[2], rm[5], rm[8], 0.0f,
                       tm[0], tm[1], tm[2], 1.0f);
    return m;
//    return glm::rotate(m2, angle, axis);
}

glm::mat4 makeCameraMatrixFromBoardPose(const cv::Mat rotation, const cv::Mat translation){
    auto m = matrixFromRvecTvec(rotation, translation);
//    float angle = PI;
//    glm::vec3 axis = glm::vec3(1, 0, 0);
    glm::mat4 m2 = glm::inverse(m);
    return m2;
//    return glm::rotate(m2, angle, axis);
}
}
