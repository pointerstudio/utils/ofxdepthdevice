#ifndef STRUKTUREN_H
#define STRUKTUREN_H

#include "ofMain.h"
#include "ofxCv.h"
#include <opencv2/aruco/charuco.hpp>

namespace CalibrationHelper {
/*
 * in and output data of calibration functions
 */

struct IntrinsicsCalibrationData {
    bool isCalibrated = false;
    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;
    vector <cv::Mat> rvecs, tvecs;
    double repError;
};

struct ExtrinsicsPoseData {
    bool isCalibrated = false;
    cv::Mat rvec, tvec;
};
struct ExtrinsicsCalibrationData {
    ExtrinsicsPoseData boardPose;
    glm::mat4 cameraPoseMatrix;
    bool hasTransformation;
    glm::mat4 cameraTransformationMatrix;
};
struct ExtrinsicsDetectedBoard {
    cv::Ptr <cv::aruco::CharucoBoard> charucoBoard;
    cv::Ptr <cv::aruco::DetectorParameters> detectorParams;
    ExtrinsicsCalibrationData extrinsicsCalibrationData;
};
struct ExtrinsicsDetectedBoardsOnImage {
    vector <ExtrinsicsDetectedBoard> detectedBoards;
    cv::Mat image;
    std::chrono::microseconds timestamp;
};

struct CommonData {
    cv::Ptr <cv::aruco::CharucoBoard> charucoBoard;
    cv::Ptr <cv::aruco::Board> board;
    cv::Ptr <cv::aruco::DetectorParameters> detectorParams;

    cv::Mat charucoBoardImage;
    ofImage charucoBoardOfImage;
    ofImage detectedBoardIntrinsics;
    ofImage detectedBoardExtrinsics;

    cv::Mat detectedBoardPositionsIntrinsics;
    ofImage ofDetectedBoardPositionsIntrinsics;
    bool isIntrinsicsBoardDetected = false;

    IntrinsicsCalibrationData intrinsics;
    ExtrinsicsCalibrationData extrinsics;

    string id;
    string saveLoadPath;
};

struct CommonDataMultiBoard : public CommonData {

    vector <ExtrinsicsDetectedBoardsOnImage> detectedBoardsOnImages;

};

struct CalibrationBoardParameters {
    cv::aruco::PREDEFINED_DICTIONARY_NAME predefinedDictionaryName;
    int squaresX;
    int squaresY;
    float squareLength;
    float markerLength;
    int boardPixelWidth;
    int boardPixelHeight;
    int offset;
};

struct CalibratorIntrinsicsParameters {
    bool showChessboardCorners;
    bool refineStrategy;
};

const CalibrationBoardParameters CALIBRATIONBOARD_PARAMETERS_A4 = CalibrationBoardParameters({
        cv::aruco::DICT_6X6_50, // dictionaryName
        8, // squaresX
        5, // squaresY
        35, // squareLength in mm
        17.5, // markerLength in mm
        1728, // boardPixelWidth
        1080, // boardPixelHeight
        0
    });

const CalibrationBoardParameters CALIBRATIONBOARD_PARAMETERS_A2 = CalibrationBoardParameters({
        cv::aruco::DICT_6X6_50, // dictionaryName
        8, // squaresX
        5, // squaresY
        69, // squareLength in mm
        34, // markerLength in mm
        1728, // boardPixelWidth
        1080, // boardPixelHeight
        0
    });

const CalibrationBoardParameters CALIBRATIONBOARD_PARAMETERS_A2_MULTI = CalibrationBoardParameters({
        cv::aruco::DICT_6X6_250, // dictionaryName
        8, // squaresX
        5, // squaresY
        74, // squareLength in mm
        37, // markerLength in mm
        1728, // boardPixelWidth
        1080, // boardPixelHeight
        0
    });

const CalibrationBoardParameters CALIBRATIONBOARD_PARAMETERS_FAKE = CalibrationBoardParameters({
        cv::aruco::DICT_6X6_50, // dictionaryName
        8, // squaresX
        5, // squaresY
        216, // squareLength
        108, // markerLength
        1728, // boardPixelWidth
        1080, // boardPixelHeight
        0
    });

const CalibrationBoardParameters CALIBRATIONBOARD_PARAMETERS_DEFAULT = CALIBRATIONBOARD_PARAMETERS_A4;

const CalibratorIntrinsicsParameters CALIBRATORINSTRINSICS_PARAMETERS_DEFAULT = {
    false, true
};
}
#endif // STRUKTUREN_H
