#ifndef CALIBRATIONUTILS_H
#define CALIBRATIONUTILS_H

#include "Strukturen.h"

namespace CalibrationHelper {

void generateCharucoBoard(const int squaresX,
                          const int squaresY,
                          const float squareLength,
                          const float markerLength,
                          const cv::Ptr <cv::aruco::Dictionary> dictionary,
                          const int boardPixelWidth,
                          const int boardPixelHeight,
                          cv::Ptr <cv::aruco::CharucoBoard> & charucoBoard,
                          cv::Ptr <cv::aruco::Board> & board,
                          cv::Mat & charucoBoardImage,
                          const int offset = 0);
void generateCharucoBoard(const CalibrationBoardParameters & p, CommonData & commonData);
void drawCharucoBoard(const cv::Ptr <cv::aruco::CharucoBoard> & charucoBoard,
                      const int boardPixelWidth,
                      const int boardPixelHeight,
                      cv::Mat & charucoBoardImage);
string getCharucoBoardFileName(const cv::Ptr <cv::aruco::CharucoBoard> & charucoBoard);
glm::mat4 matrixFromRvecTvec(const cv::Mat rvec, const cv::Mat tvec);
glm::mat4 makeCameraMatrixFromBoardPose(const cv::Mat rotation, const cv::Mat translation);

}

#endif // CALIBRATIONUTILS_H
