#include "CalibrationHelper.h"

namespace CalibrationHelper {
void Helper::setup(const string id, const CalibrationBoardParameters cbp){
    calibrationBoardParameters = cbp;
    calibratorIntrinsics.setup();

    commonData.detectorParams = cv::aruco::DetectorParameters::create();
    commonData.id = id;
    commonData.saveLoadPath = "calibration/" + commonData.id;


    cout << "commonData.saveLoadPAth" << commonData.saveLoadPath << endl;
    ofDirectory::createDirectory(commonData.saveLoadPath, true, true);
    generateBoard(); // generating board is too much effort to load from disk...
    loadIntrinsicsFromDisk();

    string extrinsicsFileName = commonData.saveLoadPath + "/extrinsics.json";
    if(!CalibratorExtrinsics::loadFromDisk(extrinsicsFileName, commonData.extrinsics.cameraPoseMatrix)){
        commonData.extrinsics.cameraPoseMatrix = glm::mat4(); // make identity matrix
    }
}

bool Helper::loadIntrinsicsFromDisk(string fileName){
    string intrinsicsFilePath = commonData.saveLoadPath + "/" + fileName;
    ofFile intrinsicsFile(intrinsicsFilePath);
    if(intrinsicsFile.exists()){
        if(calibratorIntrinsics.loadCameraParams(intrinsicsFilePath, commonData)){
            commonData.intrinsics.isCalibrated = true;
            ofLogNotice("CalibrationHelper::Helper::loadFromDisk") << "loaded intrinsics " << commonData.id;
        }else{
            ofLogError("CalibrationHelper::Helper::loadFromDisk") << " failed loaded intrinsics: broken file" << commonData.id;
            return false;
        }
    }else{
        ofLogError("CalibrationHelper::Helper::loadFromDisk") << " failed loaded intrinsics: no file" << commonData.id;
        return false;
    }
//    string extrinsicsFilePath = commonData.saveLoadPath + "/extrinsics.yml";
//    ofFile extrinsicsFile(extrinsicsFilePath);
//    if(extrinsicsFile.exists()){
//        if(calibratorExtrinsics.loadCameraParams(extrinsicsFilePath, commonData)){
//            commonData.extrinsics.cameraPose.isCalibrated = true;
//        }
//    }
    return true;
}
bool Helper::saveIntrinsicsToDisk(string fileName){
    string intrinsicsFilePath = commonData.saveLoadPath + "/" + fileName;
    ofFile intrinsicsFile(intrinsicsFilePath);
    if(commonData.intrinsics.isCalibrated){
        calibratorIntrinsics.saveCameraParams(intrinsicsFilePath, commonData);
        ofLogNotice("CalibrationHelper::Helper::saveToDisk") << "saved intrinsics " << commonData.id;
    }else{
        ofLogError("CalibrationHelper::Helper::saveToDisk") << " failed to save intrinsics: not calibrated " << commonData.id;
        return false;
    }

    return true;
}

bool Helper::loadExtrinsicsFromDisk(string fileName){
    return false;
}

bool Helper::saveExtrinsicsToDisk(string fileName){
    return false;
//    string intrinsicsFilePath = commonData.saveLoadPath + "/" + fileName;
//    ofFile intrinsicsFile(intrinsicsFilePath);
//    if(commonData.extrinsics.hasTransformation){
//        ofLogNotice("CalibrationHelper::Helper::saveToDisk") << "loaded intrinsics " << commonData.id;
//    }else{
//        ofLogError("CalibrationHelper::Helper::saveToDisk") << " failed loaded intrinsics: broken file" << commonData.id;
//        return false;
//    }

//    return true;
}

void Helper::generateBoard(bool saveImage){
    generateCharucoBoard(calibrationBoardParameters, commonData);

    if(saveImage){
        string boardFileName = getCharucoBoardFileName(commonData.charucoBoard);
        string boardFilePath = commonData.saveLoadPath + "/" + boardFileName;
        if(!ofFile(boardFilePath).exists()){
//            ofFileDialogResult result = ofSystemSaveDialog(boardFilePath, "save charuco board image");
//            if(result.bSuccess){
            commonData.charucoBoardOfImage.save(commonData.saveLoadPath + "/" + boardFileName);
//            }
        }else{
            ofLogNotice("generateBoard") << commonData.id << ": board image file already exists";
        }
    }

    std::cout << "charuco: " <<  commonData.charucoBoardOfImage.getWidth() << "x" << commonData.charucoBoardOfImage.getHeight() << endl;
}

void Helper::intrinsicsDetectBoard(ofPixels & pixels){
    cv::Mat image = ofxCv::toCv(pixels);
    calibratorIntrinsics.detectBoardOnFrame(image, commonData);
}

void Helper::intrinsicsCalibrateCamera(bool saveCalibration){
    calibratorIntrinsics.calibrate(commonData);
    if(saveCalibration){
        calibratorIntrinsics.saveCameraParams(commonData.saveLoadPath + "/intrinsics.yml", commonData);
    }
}

void Helper::extrinsicsGetCameraPose(ofPixels & pixels){
    cv::Mat image = ofxCv::toCv(pixels);
    calibratorExtrinsics.detectPoseOnFrame(image, commonData);
}

void Helper::draw(float x, float y){
    drawDetectionBoards(x, y);
}

void Helper::drawDetectionBoards(float x, float y){
    int w = 320;
    int h = 320 * commonData.charucoBoardOfImage.getHeight() / commonData.charucoBoardOfImage.getWidth();
    int margin = 20;
    if(commonData.charucoBoardOfImage.isAllocated()){
        ofDrawBitmapStringHighlight("charucoBoard", x, y);
        commonData.charucoBoardOfImage.draw(x, y, w, h);
        y += h + margin;
    }
    if(commonData.detectedBoardIntrinsics.isAllocated()){
        ofDrawBitmapStringHighlight("detected board", x, y);
        commonData.detectedBoardIntrinsics.draw(x, y, w, h);
        y += h + margin;
    }
    if(commonData.ofDetectedBoardPositionsIntrinsics.isAllocated()){
        ofDrawBitmapStringHighlight("detected board intrinsics", x, y);
        commonData.ofDetectedBoardPositionsIntrinsics.draw(x, y, w, h);
        y += h + margin;
    }
    if(commonData.intrinsics.isCalibrated){
        std::stringstream distCoeffsS;
        distCoeffsS << fixed << commonData.intrinsics.distCoeffs;
        string distCoeffs = distCoeffsS.str();
        ofStringReplace(distCoeffs, " ", "\n");
        std::stringstream info;
        info << "calibrated board\n\n";
        info << "camera matrix\n" << commonData.intrinsics.cameraMatrix << "\n\n";
        info << "distance coefficients\n" << commonData.intrinsics.distCoeffs;
        ofDrawBitmapStringHighlight(info.str(), x, y);
        y += (3 * 20) + margin;
    }
}

/**
 * @brief MultiHelper::charucoBoards
 *
 * static variables
 */
vector <cv::Ptr <cv::aruco::CharucoBoard> > MultiHelper::sCharucoBoards;
vector <cv::Ptr <cv::aruco::Dictionary> > MultiHelper::sDictionaries;
cv::Ptr <cv::aruco::DetectorParameters> MultiHelper::sDetectorParameters;
vector <ofImage> MultiHelper::sCharucoBoardOfImages;
ofDirectory MultiHelper::sStaticDirectory;
MultiBufferThread <map <int, MultiHelper::ExtrinsicsCalibrationResult>, MultiHelper::UCR_NUM> MultiHelper::ueberCalibrationWorker;
std::atomic <bool> MultiHelper::sUeberCalibrating;

void MultiHelper::setupStatic(const vector <CalibrationBoardParameters> cbps,
                              const string staticDirectoryPath){

    vector <cv::aruco::PREDEFINED_DICTIONARY_NAME> dictionaryNames;
    for(auto & c : cbps){
        cv::Ptr <cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(c.predefinedDictionaryName);
        if(std::find(dictionaryNames.begin(), dictionaryNames.end(), c.predefinedDictionaryName) == dictionaryNames.end()){
            dictionaryNames.push_back(c.predefinedDictionaryName);
            sDictionaries.push_back(dictionary);
        }
        cv::Ptr <cv::aruco::CharucoBoard> charucoBoard;
        cv::Ptr <cv::aruco::Board> board;
        cv::Mat charucoBoardImage;

        charucoBoard = cv::aruco::CharucoBoard::create(
            c.squaresX,
            c.squaresY,
            c.squareLength,
            c.markerLength,
            dictionary);

        for(int i = 0; i < int(charucoBoard->ids.size()); i++){
            charucoBoard->ids[i] += c.offset;
        }

        drawCharucoBoard(charucoBoard,
                         c.boardPixelWidth,
                         c.boardPixelHeight,
                         charucoBoardImage);

        ofImage charucoBoardOfImage;
        ofxCv::toOf(charucoBoardImage, charucoBoardOfImage);

        sCharucoBoards.push_back(charucoBoard);
        sCharucoBoardOfImages.push_back(charucoBoardOfImage);
    }
    sDetectorParameters = cv::aruco::DetectorParameters::create();
    if(staticDirectoryPath != ""){
        sStaticDirectory = ofDirectory(staticDirectoryPath);
        if(!sStaticDirectory.exists()){
            sStaticDirectory.create(true);
        }
        for(int i = 0; i < sCharucoBoards.size(); i++){
            string boardImageFilename = ofToString(i) + "_" + ofToString(cbps[i].offset) + "_" + getCharucoBoardFileName(sCharucoBoards[i]);
            ofFile boardFile(sStaticDirectory.getAbsolutePath() + "/" + boardImageFilename);
            if(!boardFile.exists()){
                sCharucoBoardOfImages[i].save(boardFile);
                cout << "save " << boardImageFilename << endl;
            }
        }
    }
}

void MultiHelper::setup(const string _id){
    id = _id;
    string intrinsicsFilePath = "calibration/" + id + "/intrinsics.yml";
    ofFile intrinsicsFile(intrinsicsFilePath);
    if(intrinsicsFile.exists()){
        cv::Size imageSize;
        int flags;
        if(CalibratorIntrinsics::loadCameraParams(intrinsicsFilePath, imageSize, flags,
                                                  intrinsics.cameraMatrix, intrinsics.distCoeffs, intrinsics.repError)){
            intrinsics.isCalibrated = true;
            ofLogNotice("CalibrationHelper::MultiHelper::setup") << "loaded intrinsics " << id;
        }else{
            ofLogError("CalibrationHelper::MultiHelper::setup") << " failed loading intrinsics: broken file" << id;
        }
    }else{
        ofLogError("CalibrationHelper::MultiHelper::setup") << " failed loading intrinsics: no file" << id;
    }
}

bool MultiHelper::getBoardPoses(const cv::Mat & mat,
                                ofImage & debugPixels){
    if(intrinsics.isCalibrated){
        cv::Mat debugImage;
        if(!debugPixels.isAllocated()){
            debugPixels.allocate(mat.size().width, mat.size().height, OF_IMAGE_COLOR);
        }else{
            debugPixels.clear();
        }
        if(getBoardPoses(mat, debugImage)){
            ofxCv::toOf(debugImage, debugPixels);
            debugPixels.update();
            return true;
        }
    }else{
        ofLogError("MultiHelper::getBoardPose") << "intrinsics not calibrated" << endl;
    }
    return false;
}
bool MultiHelper::getBoardPoses(const cv::Mat & mat,
                                cv::Mat & debugImage){
    if(intrinsics.isCalibrated){
        debugImage = cv::Mat(); // clear contents
        cvtColor(mat, debugImage, cv::COLOR_BGRA2RGB);
        map <int, cv::Mat> rvecs, tvecs;
        if(CalibratorExtrinsics::detectPosesOnFrame(
               mat,
               intrinsics.cameraMatrix,
               intrinsics.distCoeffs,
               sCharucoBoards,
               sDictionaries[0],
               sDetectorParameters,
               true,
               debugImage,
               rvecs,
               tvecs
               )){
            return true;
        }
    }else{
        ofLogError("MultiHelper::getBoardPose") << "intrinsics not calibrated" << endl;
    }
    return false;
}

}
