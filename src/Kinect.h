#pragma once

#include "PreprocessorConfig.h"
#ifdef K4A
 #include <k4a/k4a.hpp>
 #include "ofMain.h"
 #include "MultiBufferThreads.h"
 #include "MultiBufferThread.h"
 #include "DepthDevice.h"
 #include <thread>
 #include <mutex>

    namespace ofxDepthDevice {
    #define K4A_IMU_SAMPLE_RATE 1666 // +/- 2%

    class Kinect : public DepthDevice {
        public:

            enum FRAME_READERS {
                FR_XYZ = 0,
                FR_SCAN = 0,
                FR_NUM
            };

            enum XYZ_READERS {
                XR_VBO = 0,
                XR_GET_CALIBRATION,
                XR_NUM
            };

            enum VBO_READERS {
                VR_DRAW = 0,
                VR_GET_VBO,
                VR_GET_IMAGE,
                VR_GET_CV_IMAGE,
                VR_NUM
            };

            enum SCAN_READERS {
                SC_DRAW = 0,
                SC_NUM
            };

            struct FrameSet {
                k4a::image color_image;
                k4a::image depth_image;
            };

            struct XyzSet {
                k4a::image color_image;
                k4a::image color_image_transformed;
                k4a::image xyz_image;
            };

            struct VboSet {
                ofVboMesh pointcloud;
                k4a::image color_image;
//                ofImage color_image_of;
//                ofImage color_image_of_thumb;
                cv::Mat color_image_cv;
            };

            Kinect();
            virtual ~Kinect();

            virtual void setup() override;
            virtual void setup(const uint32_t index, const k4a_device_configuration_t = K4A_DEVICE_CONFIG_OFXK4AMINI_DEFAULT, bool synchronousInitialization = false);
            virtual void update() override;
            virtual void draw(bool fresh = true) override;
            virtual void close() override;
            virtual bool readIntrinsics(cv::Mat & cameraMatrix, cv::Mat & distCoeffs) override;
            const virtual ofVboMesh & getVboMesh(bool fresh = true) override;
            virtual ofImage getColorImage(bool fresh = true) override;
            virtual ofImage getColorImageThumb(bool fresh = true) override;
            const virtual cv::Mat & getCalibrationColorCv(bool fresh = true) override;
            virtual void requestScan() override;
            const virtual bool getScan(ofVboMesh & scan, ofImage & colorImage) override;

            k4a::device device;

            virtual glm::mat4 getColorToDepth() override;
            k4a::calibration getCalibration();

        protected:
            void initialize_sensor();
            void initialize_threads();
            void update_frame(k4a::capture & c, k4a::image & c_i, k4a::image & d_i);
            bool update_point_cloud(const k4a::image & xyz_i, const k4a::image & c_i, ofVboMesh & xyz_vbo);
            void show_point_cloud(const ofVboMesh & xyz_vbo);

            k4a::calibration calibration;
            k4a::capture capture;
            std::mutex transformation_mtx;
            k4a::transformation transformation;
            k4a_device_configuration_t device_configuration;
            uint64_t minimumFrameTimeMicros = 1000000 / 10;
            uint64_t lastFrame = 0;

            uint32_t kinect_device_index;

            std::thread initializer;

            MultiBufferThread <FrameSet, FR_NUM> frameWorker;
            MultiBufferThread <XyzSet, XR_NUM> xyzWorker;
            MultiBufferThread <VboSet, VR_NUM> vboWorker;
            MultiBufferThread <VboSet, SC_NUM> scanWorker;
            atomic <bool> scanRequested;

            SubThread vboSubPoints;
            SubThread vboSubImages;

//            std::mutex calibration_mtx;
//            DeviceCalibrationFrame deviceCalibrationFrame;
    };
    }

#endif
