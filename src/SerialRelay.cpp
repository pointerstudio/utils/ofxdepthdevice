#include "SerialRelay.h"

SerialRelay::SerialRelay(){

}

void SerialRelay::setup(){
    serial.listDevices();
    vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();

    int id = -1;
    for(auto device : deviceList){
        cout << ": id:" << device.getDeviceID() << " name:" << device.getDeviceName() << " path:" << device.getDevicePath() << endl;
        if(ofIsStringInString(device.getDeviceName(), "ACM")){
            id = device.getDeviceID();
            break;
        }
    }
    if(id == -1){
        cout << "whoops, no device found... trying with the first one" << endl;
        id = 0;
    }
    setup(id);
}

void SerialRelay::setup(int deviceID){
    serial.setup(deviceID, BAUD);
    string s = "SerialRelay::setup(" + ofToString(deviceID) + ")";
    ofLogNotice(s) << "executed";
}

void SerialRelay::reset(){
    uint64_t start = ofGetElapsedTimeMicros();
    uint64_t setoff = start;
    uint64_t seton = setoff + RESET_TIMER;
    uint64_t done = seton + RESET_TIMER + RESET_TIMER;
    while(ofGetElapsedTimeMicros() < setoff){
        // do nothing
    }
    set(HOST_COMMAND_OFF);
    while(ofGetElapsedTimeMicros() < seton){
        // do nothing
    }
    set(HOST_COMMAND_ON);
    while(ofGetElapsedTimeMicros() < done){
        // do nothing
    }
}

void SerialRelay::set(unsigned char cmd){
    serial.writeByte(MARKER_START);
    serial.writeByte(cmd);
    serial.writeByte(MARKER_STOP);
    string msg = cmd == HOST_COMMAND_ON ? "on" : "off";
    ofLogNotice("SerialRelay::set()") << msg << " time " << ofToString(ofGetElapsedTimeMicros());
    int border = 50;
    int x = ofRandom(border, ofGetWidth() - border);
    int y = ofRandom(border, ofGetHeight() - border);
//    ofDrawBitmapString(msg, x, y);
//    glColor4f(1.0, 0.0, 0.0, 1.0);
//    glBegin(GL_TRIANGLE_FAN);
//    glVertex2i(x, y);
//    glVertex2i(x + border, y);
//    glVertex2i(x + border, y + border);
//    glVertex2i(x, y + border);
//    glEnd();
}
