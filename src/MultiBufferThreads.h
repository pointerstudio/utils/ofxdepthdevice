#pragma once

#include <thread>
#include <atomic>

template <class T>
class TrippleBufferThread {
    public:
        T buffers[3];
        std::thread worker;
        std::atomic <bool> is_thread_running;
        std::atomic <bool> is_requested;
        std::atomic <bool> is_fresh;
        std::atomic <int> FRESH_INDEX;
        std::atomic <int> READ_INDEX;
        void setup(){
            is_thread_running.store(true);
            is_fresh.store(false);
            is_requested.store(false);
            FRESH_INDEX.store(-1);
            READ_INDEX.store(-1);
        }
        int getNextWriteIndex(){
            int stale = FRESH_INDEX.load();
            int read = READ_INDEX.load();
            int read_incremented = (read + 1) % 3;
            int write = read_incremented;
            if(write == stale){
                int read_doubleincremented = (read + 2) % 3;
                write = read_doubleincremented;
            }
            return write;
        }
        void setFreshIndex(int write){
            is_fresh.store(true);
            is_requested.store(false);
            FRESH_INDEX.store(write);
        }
        int getFreshReadIndex(){
            int read = FRESH_INDEX.load();
            READ_INDEX.store(read);
            return read;
        }
        int getCurrentReadIndex(){
            return READ_INDEX.load();
        }
        void close(){
            is_thread_running.store(false);
            worker.join();
        }
};

template <class T>
class QuadroBufferThread {
    public:
        T buffers[4];
        std::thread worker;
        std::atomic <bool> is_thread_running;
        std::atomic <bool> is_requested;
        std::atomic <bool> is_fresh;
        std::atomic <int> FRESH_INDEX;
        std::atomic <int> READ_INDEX_0;
        std::atomic <int> READ_INDEX_1;
        void setup(){
            is_thread_running.store(true);
            is_fresh.store(false);
            is_requested.store(false);
            FRESH_INDEX.store(-1);
            READ_INDEX_0.store(-1);
            READ_INDEX_1.store(-1);
        }
        int getNextWriteIndex(){
            int stale = FRESH_INDEX.load();
            int read_0 = READ_INDEX_0.load();
            int read_1 = READ_INDEX_1.load();
            int write = (stale + 1) % 4;
            while(write == read_0 || write == read_1){
                write = (write + 1) % 4;
            }
            return write;
        }
        void setFreshIndex(int write){
            is_fresh.store(true);
            is_requested.store(false);
            FRESH_INDEX.store(write);
        }
        int getFreshReadIndex(int reader = 0){
            int read = FRESH_INDEX.load();
            if(reader == 0){
                READ_INDEX_0.store(read);
            }else if(reader == 1){
                READ_INDEX_1.store(read);
            }
            return read;
        }
        void close(){
            is_thread_running.store(false);
            worker.join();
        }
};
