#include "Kinect.h"

#ifdef K4A
 #include <glm/gtx/matrix_decompose.hpp>
 #include <chrono>

    namespace ofxDepthDevice {
// Constructor
    Kinect::Kinect(){
        depthDeviceType = DepthDeviceType::OFXDD_K4A;
    }

    Kinect::~Kinect(){
        if(is_initialized.load()){
            ofLogNotice("ofxDepthDevice") << "automatically close Kinect";
            close();
        }
    }

    void Kinect::setup(){
        setup(K4A_DEVICE_DEFAULT);
    }

    void Kinect::setup(const uint32_t index, const k4a_device_configuration_t configuration, bool synchronousInitialization){
        kinect_device_index = index;
        device_configuration = configuration;

        if(synchronousInitialization){
            initialize_sensor();
            initialize_threads();
        }else{
            initializer = std::thread([&] {
                initialize_sensor();
                initialize_threads();
            });
        }
    }

// Initialize Sensor
    void Kinect::initialize_sensor(){
        OFX_PROFILER_FUNCTION();
        // Get Connected Devices
        const int32_t device_count = k4a::device::get_installed_count();
        try{
            if(device_count == 0){
                throw k4a::error("Failed to find device!");
            }
            // Open Default Device
            device = k4a::device::open(kinect_device_index);

            // configure the configuration with configurements
            device_configuration.disable_streaming_indicator = true;

            // Start Cameras with Configuration
            device.start_cameras(&device_configuration);

            // Get Calibration
            calibration = device.get_calibration(device_configuration.depth_mode, device_configuration.color_resolution);

            // Create Transformation
            transformation = k4a::transformation(calibration);

            // depth unit is millimeters
            // https://docs.microsoft.com/en-us/azure/kinect-dk/coordinate-systems
            depthUnitScale = glm::vec3(1);

            ofLogNotice() << "initialized sensor " << kinect_device_index << endl;
            is_initialized.store(true);
        }
        catch(k4a::error e){
            ofLogError() << e.what();
            is_initialized.store(false);
        }
    }

    void Kinect::initialize_threads(){
        if(is_initialized.load()){
            serial_number = device.get_serialnum();
            ofLogNotice("ofxDepthDevice") << "SERIAL NUMBER:" << serial_number << endl;

            is_updating.store(true);

            frameWorker.setup();
            frameWorker.worker = std::thread([&] {
                while(frameWorker.is_thread_running){
                    uint64_t now = ofGetElapsedTimeMicros();
                    if(is_updating.load()){
                        if(minimumFrameTimeMicros > 0 && (now - lastFrame > minimumFrameTimeMicros || now < lastFrame)){
                            //if(!frameWorker.isFresh() && is_updating.load()){
                            OFX_PROFILER_SCOPE("frame worker thread");
                            auto & writer = frameWorker.getNextWriteBuffer();
                            {
                                OFX_PROFILER_SCOPE("update frame");
                                update_frame(capture,
                                             writer.color_image,
                                             writer.depth_image);
                            }
                            frameWorker.setFresh();
                            lastFrame = now;
                            //}else{
                            //std::this_thread::sleep_for(10ms);
                            //}
                        }else{
                            std::this_thread::sleep_for(10ms);
                        }
                    }else{
                        std::this_thread::sleep_for(10ms);
                    }
                }
            });

            xyzWorker.setup();
            xyzWorker.worker = std::thread([&] {
                while(xyzWorker.is_thread_running){
                    if(frameWorker.isFresh(FR_XYZ)){
                        OFX_PROFILER_SCOPE("xyz worker thread");
                        auto & writer = xyzWorker.getNextWriteBuffer();
                        const auto & reader = frameWorker.getFreshReadBuffer(FR_XYZ);
                        if(reader.color_image.handle()
                           && reader.depth_image.handle()){
                            OFX_PROFILER_SCOPE("image transform");
                            {
                                OFX_PROFILER_SCOPE("color_image_to_depth_camera");
                                if(writer.color_image_transformed.is_valid()){
                                    transformation.color_image_to_depth_camera(reader.depth_image,
                                                                               reader.color_image,
                                                                               &writer.color_image_transformed);
                                }else{
                                    writer.color_image_transformed = transformation.color_image_to_depth_camera(reader.depth_image,
                                                                                                                reader.color_image);
                                }
                            }
                            {
                                OFX_PROFILER_SCOPE("copy the image?");
                                writer.color_image = reader.color_image;
                            }
                            {
                                OFX_PROFILER_SCOPE("depth image to pointcloud");
                                writer.xyz_image = transformation.depth_image_to_point_cloud(reader.depth_image,
                                                                                             K4A_CALIBRATION_TYPE_DEPTH);
                            }
                            xyzWorker.setFresh();
                        }
                    }else{
                        std::this_thread::sleep_for(10ms);
                    }
                }
            });

            vboSubPoints.setup();
            vboSubPoints.worker = std::thread([&] {
                while(vboSubPoints.is_thread_running.load()){
                    if(vboSubPoints.has_work.load()){
                        auto & reader = xyzWorker.getCurrentReadBuffer(XR_VBO);
                        auto & writer = vboWorker.getCurrentWriteBuffer();
                        update_point_cloud(reader.xyz_image,
                                           reader.color_image_transformed,
                                           writer.pointcloud);
                        vboSubPoints.has_work.store(false);
                    }else{
                        std::this_thread::sleep_for(10ms);
                    }
                }
            });

            vboSubImages.setup();
            vboSubImages.worker = std::thread([&] {
                while(vboSubImages.is_thread_running.load()){
                    if(vboSubImages.has_work.load()){
                        OFX_PROFILER_SCOPE("sub worker thread Images");
                        auto & reader = xyzWorker.getCurrentReadBuffer(XR_VBO);
                        auto & writer = vboWorker.getCurrentWriteBuffer();

                        auto & ci = writer.color_image;
                        auto & ci_cv = writer.color_image_cv;

                        ci = reader.color_image;
                        int w = ci.get_width_pixels();
                        int h = ci.get_height_pixels();


                        // k4a to cv
                        ci_cv = cv::Mat(h,
                                        w,
                                        CV_8UC4,
                                        (void *)ci.get_buffer(),
                                        cv::Mat::AUTO_STEP);

//                            // k4a to of
//                            auto & ci_of = writer.color_image_of;
//                            auto & ci_of_thumb = writer.color_image_of_thumb;

//                            ofImage of_color_image;
//                            of_color_image.setUseTexture(false);
//                            of_color_image.update();
//                            k4a::toOf(ci, of_color_image);

//                            ci_of = of_color_image;
//                            ci_of.update();

//                            ci_of_thumb.setUseTexture(false);
//                            k4a::toOf(ci, ci_of_thumb);
//                            ci_of_thumb = of_color_image;
//                            ci_of_thumb.resize(w / 8, h / 8);
//                            ci_of_thumb.update();

                        vboSubImages.has_work.store(false);
                    }else{
                        std::this_thread::sleep_for(10ms);
                    }
                }
            });

            vboWorker.setup();
            vboWorker.worker = std::thread([&] {
                while(vboWorker.is_thread_running.load()){
                    if(xyzWorker.isFresh(XR_VBO)){
                        OFX_PROFILER_SCOPE("vbo worker thread");

                        // we create the new read and write indices for our buffers
                        // in the subthreads we can use them when calling getCurrent<....>Buffer
                        auto & reader = xyzWorker.getFreshReadBuffer(XR_VBO);
                        auto & writer = vboWorker.getNextWriteBuffer();

                        // aaand now we did all we need, so we can tell our sub threads to get work done
                        vboSubPoints.has_work.store(true);
                        vboSubImages.has_work.store(true);

                        std::this_thread::sleep_for(10ms);
                        while(vboSubPoints.has_work.load() || vboSubImages.has_work.load()){
                            std::this_thread::sleep_for(10ms);
                        }
                        vboWorker.setFresh();

                    }else{
                        std::this_thread::sleep_for(10ms);
                    }
                }
            });
            scanRequested.store(false);
            scanWorker.setup();
            scanWorker.worker = std::thread([&] {
                while(scanWorker.is_thread_running.load()){
                    if(scanRequested.load() && !scanWorker.isFresh()){
                        const auto & reader = frameWorker.getFreshReadBuffer(FR_SCAN);
                        auto & writer = scanWorker.getNextWriteBuffer();
                        if(reader.color_image.handle()
                           && reader.depth_image.handle()){
                            OFX_PROFILER_SCOPE("image transform");
                            {
                                //auto depth_image_transformed = transformation.depth_image_to_color_camera(reader.depth_image);
                                //auto xyz_image = transformation.depth_image_to_point_cloud(depth_image_transformed,
                                //K4A_CALIBRATION_TYPE_COLOR);
                                auto color_image_transformed = transformation.color_image_to_depth_camera(reader.depth_image,
                                                                                                          reader.color_image);
                                auto xyz_image = transformation.depth_image_to_point_cloud(reader.depth_image,
                                                                                           K4A_CALIBRATION_TYPE_DEPTH);
                                update_point_cloud(xyz_image, color_image_transformed, writer.pointcloud);

                            }
                            {
                                OFX_PROFILER_SCOPE("copy the image?");
                                writer.color_image = reader.color_image;
                            }
                            scanWorker.setFresh();
                            scanRequested.store(false);
                        }else{
                            std::this_thread::sleep_for(chrono::milliseconds(10));
                        }
                    }
                }
            });
        }
    }

// Update Frame
    inline void Kinect::update_frame(k4a::capture & c, k4a::image & c_i, k4a::image & d_i){
        OFX_PROFILER_FUNCTION();
        // Get Capture Frame
        constexpr std::chrono::milliseconds time_out(K4A_WAIT_INFINITE);
        const bool result = device.get_capture(&c, time_out);
        if(!result){
            this->~Kinect();
        }else{
            {
                OFX_PROFILER_SCOPE("sub capture color image");
                c_i = c.get_color_image();
            }
            {
                OFX_PROFILER_SCOPE("sub capture depth image");
                d_i = c.get_depth_image();
            }
        }
        capture.reset();
    }

// Draw Point Cloud
    inline bool Kinect::update_point_cloud(const k4a::image & xyz_i, const k4a::image & c_i, ofVboMesh & xyz_vbo){
        OFX_PROFILER_FUNCTION();
        if(!xyz_i.handle() || !c_i.handle()){
            return false;
        }

        bool success = true;
//    xyz_vbo.clear();
        {
            OFX_PROFILER_SCOPE("sub generate ofMesh pointcloud");
            glm::mat4 extrinsics = getExtrinsics();
            calibration_mtx.lock();
            ofCylinderPrimitive cropping = localCropping;
            calibration_mtx.unlock();
            success = k4a::toOf(xyz_i,
                                c_i,
                                depthUnitScale,
                                extrinsics,
                                cropping,
                                xyz_vbo);
        }

        // Release Image Handles
//    xyz_i.reset();
//    c_i.reset();
        return success;
    }
// Show Point Cloud
    inline void Kinect::show_point_cloud(const ofVboMesh & xyz_vbo){
        OFX_PROFILER_FUNCTION();
        if(xyz_vbo.getVertices().size() == 0){
            //ofLogNotice("ofxDepthDevice") << " serial: " << serial_number << ": no vertices in pointcloud";
            return;
        }
        {
            OFX_PROFILER_SCOPE("drawVertices");
//             we already apply extrinsics when computing the pointcloud
//            ofPushMatrix();
//            xyz_vbo.disableColors();
//            xyz_vbo.drawVertices();
//            xyz_vbo.enableColors();
//            ofMultMatrix(calibrationHelper.commonData.extrinsics.cameraPoseMatrix);
            xyz_vbo.drawVertices();
//            xyz_vbo.getVbo().drawElements(GL_POINTS, xyz_vbo.getVbo().getNumVertices());
//            ofPopMatrix();
        }
    }

// Update
    void Kinect::update(){
//    OFX_PROFILER_FUNCTION();
        if(initializer.joinable()){
            initializer.join();
        }
    }

    void Kinect::draw(bool fresh){
        OFX_PROFILER_FUNCTION();
        if(vboWorker.FRESH_INDEX.load() != -1){
            const auto & reader = fresh ?
                                  vboWorker.getFreshReadBuffer(VR_DRAW) : vboWorker.getCurrentReadBuffer(VR_DRAW);
            show_point_cloud(reader.pointcloud);
        }
    }

    const ofVboMesh & Kinect::getVboMesh(bool fresh){
        if(vboWorker.FRESH_INDEX.load() != -1){
            if(fresh){
                return vboWorker.getFreshReadBuffer(VR_GET_VBO).pointcloud;
            }else{
                return vboWorker.getCurrentReadBuffer(VR_GET_VBO).pointcloud;
            }
        }
        return ofVboMesh();
    }

    ofImage Kinect::getColorImage(bool fresh){
        if(vboWorker.FRESH_INDEX.load() != -1){
            if(fresh){
                auto & reader = vboWorker.getFreshReadBuffer(VR_GET_IMAGE);
                ofImage out;
                k4a::toOf(reader.color_image, out);
                return out;
            }else{
                auto & reader = vboWorker.getCurrentReadBuffer(VR_GET_IMAGE);
                ofImage out;
                k4a::toOf(reader.color_image, out);
                return out;
            }
        }
        return ofImage();
    }

    ofImage Kinect::getColorImageThumb(bool fresh){
        if(vboWorker.FRESH_INDEX.load() != -1){
            if(fresh){
                auto & reader = vboWorker.getFreshReadBuffer(VR_GET_IMAGE);
                ofImage out;
                k4a::toOf(reader.color_image, out);
                out.resize(1920 / 4, 1080 / 4);
                return out;
            }else{
                auto & reader = vboWorker.getCurrentReadBuffer(VR_GET_IMAGE);
                ofImage out;
                k4a::toOf(reader.color_image, out);
                out.resize(1920 / 4, 1080 / 4);
                return out;
            }
        }
        return ofImage();
    }

    const cv::Mat & Kinect::getCalibrationColorCv(bool fresh){
        if(vboWorker.FRESH_INDEX.load() != -1){
            if(fresh){
                return vboWorker.getFreshReadBuffer(VR_GET_CV_IMAGE).color_image_cv;
            }else{
                return vboWorker.getCurrentReadBuffer(VR_GET_CV_IMAGE).color_image_cv;
            }
        }
        return vboWorker.buffers[0].color_image_cv;
    }

    void Kinect::requestScan(){
        if(!scanRequested.load()){
            scanRequested.store(true);
        }
    }
    const bool Kinect::getScan(ofVboMesh & mesh, ofImage & colorImage){
        if(scanWorker.isFresh(SC_DRAW)){
            auto & reader = scanWorker.getFreshReadBuffer(SC_DRAW);
            mesh = std::move(reader.pointcloud);
            k4a::toOf(reader.color_image, colorImage);
            return true;
        }
        return false;
    }

    glm::mat4 Kinect::getColorToDepth(){
//        glm::mat4 m = glm::mat4();
//        auto & color_extrinsics = calibration.color_camera_calibration.extrinsics;
//        auto & depth_extrinsics = calibration.depth_camera_calibration.extrinsics;

        cv::Mat se3 =
            cv::Mat(3, 3, CV_64F, calibration.extrinsics[K4A_CALIBRATION_TYPE_COLOR][K4A_CALIBRATION_TYPE_DEPTH].rotation);
        cv::Mat r_vec = cv::Mat(3, 1, CV_64F);
        Rodrigues(se3, r_vec);
        cv::Mat t_vec =
            cv::Mat(3, 1, CV_64F, calibration.extrinsics[K4A_CALIBRATION_TYPE_COLOR][K4A_CALIBRATION_TYPE_DEPTH].translation);
        auto m = glm::mat4(se3.at <float>(0), se3.at <float>(3), se3.at <float>(6), 0.0f,
                           se3.at <float>(1), se3.at <float>(4), se3.at <float>(7), 0.0f,
                           se3.at <float>(2), se3.at <float>(5), se3.at <float>(8), 0.0f,
                           t_vec.at <float>(0),  t_vec.at <float>(1), t_vec.at <float>(2), 1.0f);

        return m;
    }
    k4a::calibration Kinect::getCalibration(){
        return calibration;
    }

//    shared_ptr <DeviceCalibrationFrame> Kinect::getDeviceCalibrationFrame(){
//        if(vboWorker.FRESH_INDEX.load() != -1){
//            calibration_mtx.lock();
//            DeviceCalibrationFrame dcf = deviceCalibrationFrame;
//            calibration_mtx.unlock();
//            return make_shared <DeviceCalibrationFrame>(dcf);
//        }
//        return make_shared <DeviceCalibrationFrame>();
//    }

    bool Kinect::readIntrinsics(cv::Mat & cameraMatrix, cv::Mat & distCoeffs){
        k4a_calibration_intrinsic_parameters_t & intrinsics = calibration.color_camera_calibration.intrinsics.parameters;
        return k4a::intrinsicsToCv(intrinsics, cameraMatrix, distCoeffs);
    }

// Finalize
    void Kinect::close(){
        OFX_PROFILER_FUNCTION();
        cout << "close Kinect " << serial_number << endl;
        frameWorker.close();
        xyzWorker.close();
        vboWorker.close();
        vboSubPoints.close();
        vboSubImages.close();

        transformation.destroy();
        device.stop_cameras();
        device.close();
        is_initialized.store(false);
    }
    }
#endif
