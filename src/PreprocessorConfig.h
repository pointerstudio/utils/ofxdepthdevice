#pragma once

//#if defined(__has_include) /*llvm only - query about header files being available or not*/
// #if __has_include("k4a/k4a.h")
#define K4A
// #endif
//#endif

/*
 * Microsoft sometimes lags behind with delivering Linux bodytracking SDK
 * Therefore, let's disable it if it's not there
 */
//#if defined(__has_include) /*llvm only - query about header files being available or not*/
// #if __has_include("k4abt.h")
#define K4ABT
// #endif
//#endif

/*
 * voluntary support for ofxProfiler
 * https://gitlab.com/pointerstudio/utils/ofxProfiler
 *
 * automatically detect presence and include ofxProfiler
 */
#if defined(__has_include)  /*llvm only - query about header files being available or not*/
    #if __has_include("ofxProfiler.h")
        #define PROFILER
    #endif
#endif
#ifdef PROFILER
    #define OFX_PROFILER 1
    #include "ofxProfiler.h"
#else
    #define OFX_PROFILER_BEGIN_SESSION(name, filepath)
    #define OFX_PROFILER_CONTINUE_SESSION(name, filepath)
    #define OFX_PROFILER_END_SESSION()
    #define OFX_PROFILER_SCOPE(name)
    #define OFX_PROFILER_FUNCTION()
#endif
