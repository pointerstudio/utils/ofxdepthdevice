#ifndef SAVIOUR_H
#define SAVIOUR_H

#include "ofMain.h"
#include "PreprocessorConfig.h"

namespace ofxDepthDevice {
class Saviour {
    public:
        Saviour();
        void update();
        void save(vector <ofVboMesh> meshes,
                  vector <string> names,
                  vector <ofPixels> textures,
                  string subDirectory,
                  string fileType = "obj");

        void saveThread(vector <ofVboMesh> meshes,
                        vector <string> names,
                        vector <ofPixels> textures,
                        string subDirectory,
                        string fileType = "obj");

        void saveLODs(vector <ofVboMesh> meshes,
                      vector <string> names,
                      string subDirectory,
                      string fileType = "obj");

        void saveThreadLODs(vector <ofVboMesh> meshes,
                            vector <string> names,
                            string subDirectory,
                            string fileType = "obj");

        string getLastName();
        std::thread deed;
        std::atomic <bool> is_saving;
        std::atomic <bool> is_done;
        std::atomic <bool> is_ready;
        string saveDirectory = "models";
        string last_name;
        mutex last_name_mtx;
};
}
#endif // SAVIOUR_H
