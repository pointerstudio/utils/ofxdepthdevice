#include "Util.h"

tjhandle jpegDecompressor;

namespace k4a {
cv::Mat get_mat(k4a::image & src, bool deep_copy){
    assert(src.get_size() != 0);

    cv::Mat mat;
    const int32_t width = src.get_width_pixels();
    const int32_t height = src.get_height_pixels();

    const k4a_image_format_t format = src.get_format();
    switch(format){
     case k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_MJPG: {
         // NOTE: this is slower than other formats.
         std::vector <uint8_t> buffer(src.get_buffer(), src.get_buffer() + src.get_size());
         mat = cv::imdecode(buffer, cv::IMREAD_ANYCOLOR);
         cv::cvtColor(mat, mat, cv::COLOR_BGR2BGRA);
         break;
     }

     case k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_NV12: {
         cv::Mat nv12 = cv::Mat(height + height / 2, width, CV_8UC1, src.get_buffer()).clone();
         cv::cvtColor(nv12, mat, cv::COLOR_YUV2BGRA_NV12);
         break;
     }

     case k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_YUY2: {
         cv::Mat yuy2 = cv::Mat(height, width, CV_8UC2, src.get_buffer()).clone();
         cv::cvtColor(yuy2, mat, cv::COLOR_YUV2BGRA_YUY2);
         break;
     }

     case k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_BGRA32: {
         mat = deep_copy ? cv::Mat(height, width, CV_8UC4, src.get_buffer()).clone()
               : cv::Mat(height, width, CV_8UC4, src.get_buffer());
         break;
     }

     case k4a_image_format_t::K4A_IMAGE_FORMAT_DEPTH16:
     case k4a_image_format_t::K4A_IMAGE_FORMAT_IR16: {
         mat = deep_copy ? cv::Mat(height, width, CV_16UC1, reinterpret_cast <uint16_t *>(src.get_buffer())).clone()
               : cv::Mat(height, width, CV_16UC1, reinterpret_cast <uint16_t *>(src.get_buffer()));
         break;
     }

     case k4a_image_format_t::K4A_IMAGE_FORMAT_CUSTOM8: {
         mat = cv::Mat(height, width, CV_8UC1, src.get_buffer()).clone();
         break;
     }

     case k4a_image_format_t::K4A_IMAGE_FORMAT_CUSTOM: {
         // NOTE: This ist3 opencv_viz module format (cv::viz::WCloud).
         const int16_t * buffer = reinterpret_cast <int16_t *>(src.get_buffer());
         mat = cv::Mat(height, width, CV_32FC3, cv::Vec3f::all(std::numeric_limits <float>::quiet_NaN()));
         mat.forEach <cv::Vec3f>(
             [&](cv::Vec3f & point, const int32_t * position){
                    const int32_t index = (position[0] * width + position[1]) * 3;
                    point = cv::Vec3f(buffer[index + 0], buffer[index + 1], buffer[index + 2]);
                }
             );
         break;
     }

     default:
         throw k4a::error("Failed to convert this format!");
         break;
    }

    return mat;
}

void toOf(const k4a::image & src, ofImage & target){
    assert(src.get_size() != 0);

    const int32_t width = src.get_width_pixels();
    const int32_t height = src.get_height_pixels();
    const k4a_image_format_t format = src.get_format();
    switch(format){
     case k4a_image_format_t::K4A_IMAGE_FORMAT_DEPTH16:
     case k4a_image_format_t::K4A_IMAGE_FORMAT_IR16: {
         if(!target.isAllocated()){
             target.allocate(width, height, OF_IMAGE_GRAYSCALE);
         }
         target.getPixels().setFromPixels(src.get_buffer(), width, height, 1);
         break;
     }

     case K4A_IMAGE_FORMAT_COLOR_MJPG: {
         if(!target.isAllocated()){
             target.allocate(width, height, OF_IMAGE_COLOR);
         }
         ofPixels buffy;
         buffy.allocate(width, height, OF_PIXELS_BGRA);
         target.update();
         ofLogError("ofxK4aMini") << __FILE__ << ":" << __FUNCTION__ << " MJPEG currently not supported";
         break;
     }

     case K4A_IMAGE_FORMAT_COLOR_YUY2: {
         if(!target.isAllocated()){
             target.allocate(width, height, OF_IMAGE_COLOR);
         }
         target.getPixels().setFromPixels(src.get_buffer(), width, height, OF_PIXELS_RGBA);
         target.update();
         ofLogError("ofxK4aMini") << __FILE__ << ":" << __FUNCTION__ << " YUY2 currently not supported";
         break;
     }

     case K4A_IMAGE_FORMAT_COLOR_BGRA32: {
         if(!target.isAllocated()){
             target.allocate(width, height, OF_IMAGE_COLOR);
         }
         target.getPixels().setFromPixels(src.get_buffer(),
                                          width,
                                          height,
                                          OF_PIXELS_BGRA);
         target.update();
         break;
     }
    }
}

/*
 * practically this function costs the most, but is the least relevant
 */
//bool toOf(k4a::image xyz_image,
//          k4a::image color_image,
//          glm::vec3 & depthUnitScale,
//          ofNode & extrinsics,
//          ofVboMesh & target_pointcloud){

//    const int color_width = color_image.get_width_pixels();
//    const int color_height = color_image.get_height_pixels();
//    const int xyz_width = xyz_image.get_width_pixels();
//    const int xyz_height = xyz_image.get_height_pixels();

//    if(color_width != xyz_width || color_height != xyz_height){
//        ofLogError("ofxK4aMini") << "color and depth image need similar dimensions";
//        return false;
//    }

//    const int16_t * point_cloud_image_data = (int16_t *)(void *)xyz_image.get_buffer();
//    const k4a_image_format_t format = color_image.get_format();
//    const uint8_t * color_image_data = color_image.get_buffer();

//    target_pointcloud.clearVertices();
//    target_pointcloud.clearColors();
//    std::vector <glm::vec3> points;
//    std::vector <ofFloatColor> colors;
//    {
//        OFX_PROFILER_SCOPE("sub calculate points and colors");
//        for(int i = 0; i < color_width * color_height; i++){
//            glm::vec3 point;
//            point.x = point_cloud_image_data[3 * i + 0] * depthUnitScale.x;
//            point.y = point_cloud_image_data[3 * i + 1] * depthUnitScale.y;
//            point.z = point_cloud_image_data[3 * i + 2] * depthUnitScale.z;
//            if(point.z == 0){
//                continue;
//            }
////        auto position = extrinsics.getGlobalPosition();
////        auto orientation = extrinsics.getGlobalOrientation();
////            point = (extrinsics.getGlobalTransformMatrix()) * glm::vec4(point, 1);

//            ofColor color;
//            // color_image shall be BGRA32
//            switch(format){
//             case K4A_IMAGE_FORMAT_COLOR_YUY2:
//                 color.b = color_image_data[4 * i + 0];
//                 color.g = color_image_data[4 * i + 1];
//                 color.r = color_image_data[4 * i + 2];
//                 color.a = color_image_data[4 * i + 3];
//                 break;

//             case K4A_IMAGE_FORMAT_COLOR_MJPG:
//             case K4A_IMAGE_FORMAT_COLOR_BGRA32:
//                 color.b = color_image_data[4 * i + 0];
//                 color.g = color_image_data[4 * i + 1];
//                 color.r = color_image_data[4 * i + 2];
//                 color.a = color_image_data[4 * i + 3];
//                 break;
//            }


//            if(color.r == 0 && color.g == 0 && color.b == 0 && color.a == 0){
//                continue;
//            }
//            points.push_back(point);
//            colors.push_back(color);
//        }
//    }
//    target_pointcloud.addVertices(points);
//    target_pointcloud.addColors(colors);
//    return true;
//}

bool toOf(k4a::image xyz_image,
          k4a::image color_image,
          glm::vec3 & depthUnitScale,
          glm::mat4 & extrinsics,
          ofCylinderPrimitive & localCropping,
          ofVboMesh & target_pointcloud){

    const int color_width = color_image.get_width_pixels();
    const int color_height = color_image.get_height_pixels();
    const int xyz_width = xyz_image.get_width_pixels();
    const int xyz_height = xyz_image.get_height_pixels();

    if(color_width != xyz_width || color_height != xyz_height){
        ofLogError("ofxDepthDevice") << "color and depth image need similar dimensions";
        return false;
    }

    const int16_t * point_cloud_image_data = (int16_t *)(void *)xyz_image.get_buffer();
    const k4a_image_format_t format = color_image.get_format();
    const uint8_t * color_image_data = color_image.get_buffer();

    {
        OFX_PROFILER_SCOPE("sub prepare pointcloud");
        target_pointcloud.clearVertices();
        target_pointcloud.clearColors();
        target_pointcloud.clearTexCoords();

        target_pointcloud.getVertices().reserve(color_width * color_height);
        target_pointcloud.getColors().reserve(color_width * color_height);
        target_pointcloud.getTexCoords().reserve(color_width * color_height);
    }

    {
        OFX_PROFILER_SCOPE("sub calculate points, colors and texCoords");
        for(int i = 0; i < color_width * color_height; i++){
            glm::vec3 point;
            point.x = point_cloud_image_data[3 * i + 0] * depthUnitScale.x;
            point.y = point_cloud_image_data[3 * i + 1] * depthUnitScale.y;
            point.z = point_cloud_image_data[3 * i + 2] * depthUnitScale.z;
            if(point.z == 0){
                continue;
            }
            point = extrinsics * glm::vec4(point, 1);

            glm::vec3 pos = localCropping.getPosition();
            float height = localCropping.getHeight();
            float radius = localCropping.getRadius();
            bool doCrop = height > 0 && radius > 0;
            if(doCrop &&
               (point.x > pos.x + height / 2
                || point.x < pos.x - height / 2
                || glm::distance(point, glm::vec3(point.x, pos.y, pos.z)) > radius)
//               || point.z < 200
//               || point.z > 2000
               ){
                continue;
            }

            ofColor color;
            // color_image shall be BGRA32
            switch(format){
             case K4A_IMAGE_FORMAT_COLOR_YUY2:
                 color.b = color_image_data[4 * i + 0];
                 color.g = color_image_data[4 * i + 1];
                 color.r = color_image_data[4 * i + 2];
                 color.a = color_image_data[4 * i + 3];
                 break;

             case K4A_IMAGE_FORMAT_COLOR_MJPG:
             case K4A_IMAGE_FORMAT_COLOR_BGRA32:
                 color.b = color_image_data[4 * i + 0];
                 color.g = color_image_data[4 * i + 1];
                 color.r = color_image_data[4 * i + 2];
                 color.a = color_image_data[4 * i + 3];
                 break;
            }

            if(color.r == 0 && color.g == 0 && color.b == 0 && color.a == 0){
                continue;
            }

            glm::vec2 texCoord(
                float(i % color_width) / float(color_width),
                float(floor(i / color_width)) / float(color_height)
                );

            target_pointcloud.getVertices().push_back(point);
            target_pointcloud.getColors().push_back(color);
            target_pointcloud.getTexCoords().push_back(texCoord);
        }
    }
    return true;
}

//bool toOf(const k4a::image & xyz_image,
//          const k4a::image & color_image,
//          const glm::vec3 & depthUnitScale,
//          const ofNode & extrinsics,
//          const ofCylinderPrimitive & localCropping,
//          ofVboMesh & target_pointcloud){

//    const int color_width = color_image.get_width_pixels();
//    const int color_height = color_image.get_height_pixels();
//    const int xyz_width = xyz_image.get_width_pixels();
//    const int xyz_height = xyz_image.get_height_pixels();

//    if(color_width != xyz_width || color_height != xyz_height){
//        ofLogError("ofxDepthDevice") << "color and depth image need similar dimensions";
//        return false;
//    }

//    const int16_t * point_cloud_image_data = (int16_t *)(void *)xyz_image.get_buffer();
//    const k4a_image_format_t format = color_image.get_format();
//    const uint8_t * color_image_data = color_image.get_buffer();

//    target_pointcloud.clearVertices();
//    target_pointcloud.clearColors();
//    target_pointcloud.clearTexCoords();
//    std::vector <glm::vec3> points;
//    std::vector <ofFloatColor> colors;
//    std::vector <glm::vec2> texCoords;
//    points.reserve(color_width * color_height);
//    colors.reserve(color_width * color_height);
//    texCoords.reserve(color_width * color_height);
//    {
//        OFX_PROFILER_SCOPE("sub calculate points, colors and texCoords");
//        for(int i = 0; i < color_width * color_height; i++){
//            glm::vec3 point;
//            point.x = point_cloud_image_data[3 * i + 0] * depthUnitScale.x;
//            point.y = point_cloud_image_data[3 * i + 1] * depthUnitScale.y;
//            point.z = point_cloud_image_data[3 * i + 2] * depthUnitScale.z;
//            if(point.z == 0){
//                continue;
//            }

//            point = extrinsics.getGlobalTransformMatrix() * glm::vec4(point, 1);

//            glm::vec3 pos = localCropping.getPosition();
//            float height = localCropping.getHeight();
//            float radius = localCropping.getRadius();
//            bool doCrop = height != 0 && radius != 0;
//            if(doCrop
//               || point.x > pos.x + height / 2
//               || point.x < pos.x - height / 2
//               || glm::distance(point, glm::vec3(point.x, pos.y, pos.z)) > radius){
//                continue;
//            }

//            ofColor color;
//            // color_image shall be BGRA32
//            switch(format){
//             case K4A_IMAGE_FORMAT_COLOR_YUY2:
//                 color.b = color_image_data[4 * i + 0];
//                 color.g = color_image_data[4 * i + 1];
//                 color.r = color_image_data[4 * i + 2];
//                 color.a = color_image_data[4 * i + 3];
//                 break;

//             case K4A_IMAGE_FORMAT_COLOR_MJPG:
//             case K4A_IMAGE_FORMAT_COLOR_BGRA32:
//                 color.b = color_image_data[4 * i + 0];
//                 color.g = color_image_data[4 * i + 1];
//                 color.r = color_image_data[4 * i + 2];
//                 color.a = color_image_data[4 * i + 3];
//                 break;
//            }

//            if(color.r == 0 && color.g == 0 && color.b == 0 && color.a == 0){
//                continue;
//            }

//            glm::vec2 texCoord(
//                float(i % color_width) / float(color_width),
//                float(floor(i / color_width)) / float(color_height)
//                );

//            points.push_back(point);
//            colors.push_back(color);
//            texCoords.push_back(texCoord);
//        }
//    }
//    {
//        OFX_PROFILER_SCOPE("sub add points, colors and texCoords");
//        target_pointcloud.addVertices(points);
//        target_pointcloud.addColors(colors);
//        target_pointcloud.addTexCoords(texCoords);
//    }
//    return true;
//}

bool xyz_color_to_depth(k4a_transformation_t transformation_handle,
                        const k4a_image_t depth_image,
                        const k4a_image_t color_image,
                        k4a_image_t & transformed_color_image,
                        k4a_image_t & xyz_image){
    int depth_image_width_pixels = k4a_image_get_width_pixels(depth_image);
    int depth_image_height_pixels = k4a_image_get_height_pixels(depth_image);
    if(K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_COLOR_BGRA32,
                                                depth_image_width_pixels,
                                                depth_image_height_pixels,
                                                depth_image_width_pixels * 4 * (int)sizeof(uint8_t),
                                                &transformed_color_image)){
        printf("Failed to create transformed color image\n");
        return false;
    }

//    k4a_image_t point_cloud_image = NULL;
    if(K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
                                                depth_image_width_pixels,
                                                depth_image_height_pixels,
                                                depth_image_width_pixels * 3 * (int)sizeof(int16_t),
                                                &xyz_image)){
        printf("Failed to create point cloud image\n");
        return false;
    }

    if(K4A_RESULT_SUCCEEDED != k4a_transformation_color_image_to_depth_camera(transformation_handle,
                                                                              depth_image,
                                                                              color_image,
                                                                              transformed_color_image)){
        printf("Failed to compute transformed color image\n");
        return false;
    }

    if(K4A_RESULT_SUCCEEDED != k4a_transformation_depth_image_to_point_cloud(transformation_handle,
                                                                             depth_image,
                                                                             K4A_CALIBRATION_TYPE_DEPTH,
                                                                             xyz_image)){
        printf("Failed to compute point cloud\n");
        return false;
    }

//    tranformation_helpers_write_point_cloud(point_cloud_image, transformed_color_image, file_name.c_str());

    k4a_image_release(transformed_color_image);
//    k4a_image_release(point_cloud_image);

    return true;
}

bool xyz_depth_to_color(k4a_transformation_t transformation_handle,
                        const k4a_image_t depth_image,
                        const k4a_image_t color_image,
                        k4a_image_t & xyz_image){
    // transform color image into depth camera geometry
    int color_image_width_pixels = k4a_image_get_width_pixels(color_image);
    int color_image_height_pixels = k4a_image_get_height_pixels(color_image);
    k4a_image_t transformed_depth_image = NULL;
    if(K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_DEPTH16,
                                                color_image_width_pixels,
                                                color_image_height_pixels,
                                                color_image_width_pixels * (int)sizeof(uint16_t),
                                                &transformed_depth_image)){
        printf("Failed to create transformed depth image\n");
        return false;
    }

//    k4a_image_t point_cloud_image = NULL;
    if(K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
                                                color_image_width_pixels,
                                                color_image_height_pixels,
                                                color_image_width_pixels * 3 * (int)sizeof(int16_t),
                                                &xyz_image)){
        printf("Failed to create point cloud image\n");
        return false;
    }

    if(K4A_RESULT_SUCCEEDED !=
       k4a_transformation_depth_image_to_color_camera(transformation_handle, depth_image, transformed_depth_image)){
        printf("Failed to compute transformed depth image\n");
        return false;
    }

    if(K4A_RESULT_SUCCEEDED != k4a_transformation_depth_image_to_point_cloud(transformation_handle,
                                                                             transformed_depth_image,
                                                                             K4A_CALIBRATION_TYPE_COLOR,
                                                                             xyz_image)){
        printf("Failed to compute point cloud\n");
        return false;
    }

//    tranformation_helpers_write_point_cloud(point_cloud_image, color_image, file_name.c_str());

    k4a_image_release(transformed_depth_image);
//    k4a_image_release(point_cloud_image);

    return true;
}

bool intrinsicsToCv(const k4a_calibration_intrinsic_parameters_t & intrinsics, cv::Mat & cameraMatrix, cv::Mat & distCoeffs){
    // intrinsic parameters of the color camera
//    k4a_calibration_intrinsic_parameters_t * intrinsics = &calibration.color_camera_calibration.intrinsics.parameters;
    vector <float> _camera_matrix = {
        intrinsics.param.fx, 0.f, intrinsics.param.cx, 0.f, intrinsics.param.fy, intrinsics.param.cy, 0.f, 0.f, 1.f
    };
    cameraMatrix = cv::Mat(3, 3, CV_32F, &_camera_matrix[0]);
    vector <float> distCoeffsVector = {intrinsics.param.k1, intrinsics.param.k2, intrinsics.param.p1,
                                       intrinsics.param.p2, intrinsics.param.k3, intrinsics.param.k4,
                                       intrinsics.param.k5, intrinsics.param.k6};
    distCoeffs = cv::Mat(8, 1, CV_32F, &distCoeffsVector[0]);
    return true;
}
} // end namespace k4a

namespace ofxDepthDevice {

ofImage k4a_get_ofImage(k4a::image & src, bool deep_copy){
    cv::Mat mat = k4a::get_mat(src, deep_copy);
    ofImage image;
    image.setUseTexture(false);
    ofxCv::toOf(mat, image);
    return image;
}

cv::Mat k4a_get_mat(k4a_image_t & src, bool deep_copy){
    k4a_image_reference(src);
    k4a::image img = k4a::image(src);
    return k4a::get_mat(img, deep_copy);
}
}
