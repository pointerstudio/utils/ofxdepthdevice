#include "Saviour.h"

namespace ofxDepthDevice {
Saviour::Saviour(){
    is_saving.store(false);
    is_done.store(false);
    is_ready.store(true);
}

void Saviour::update(){
    if(!is_saving.load() && !is_ready.load() && deed.joinable()){
        deed.join();
        deed.~thread();
        is_ready.store(true);
    }
}
void Saviour::save(vector <ofVboMesh> meshes,
                   vector <string> names,
                   vector <ofPixels> textures,
                   string subDirectory,
                   string fileType){

    if(is_ready.load()){
        is_saving.store(true);
        is_ready.store(false);
        deed = std::thread(&Saviour::saveThread, this,  meshes, names, textures, subDirectory, fileType);
    }
}

void Saviour::saveThread(vector <ofVboMesh> meshes,
                         vector <string> names,
                         vector <ofPixels> textures,
                         string subDirectory,
                         string fileType){

    OFX_PROFILER_FUNCTION();
    if(fileType == "obj"){
        last_name_mtx.lock();
        last_name = subDirectory;
        last_name_mtx.unlock();
        string dirString = saveDirectory + "/" + subDirectory;

        ofDirectory dir(dirString);
        dir.create(true);

        stringstream ss_mtls[meshes.size()];
        stringstream ss_objs[meshes.size()];
        std::thread ss_threads[meshes.size() * 2];
        for(int m = 0; m < int(meshes.size()); m++){
            ss_threads[m * 2] = thread([&, m] {
                    string scopeName = "collect material and vertices " + ofToString(m);
                    OFX_PROFILER_SCOPE(scopeName.c_str());
                    string color_image_name = names[m] + "_tex_Color.png";
                    string color_image_path = dirString + "/" + color_image_name;
                    ss_mtls[m] << std::fixed
                               << "newmtl mtl_" << names[m] << "\n"
                               << "Ka 1.000 1.000 1.000\n"
                               << "Kd 1.000 1.000 1.000\n"
                               << "Ks 0.000 0.000 0.000\n"
                               << "d 1.0\n"
                               << "illum 2\n"
                               << "map_Ka " << color_image_name << "\n"
                               << "map_Kd " << color_image_name << "\n"
                               << "\n\n" << std::endl;

                    ss_objs[m] << std::fixed << "usemtl mtl_" << names[m] << "\n"
                               << "g group" << names[m] << "\n";

                    vector <glm::vec3> vertices = meshes[m].getVertices();
                    vector <ofFloatColor> colors = meshes[m].getColors();
                    vector <glm::vec2> texCoords = meshes[m].getTexCoords();

                    for(int i = 0; i < int(vertices.size()); i++){
                        ss_objs[m] << "v " << std::fixed
                                   << vertices[i].x * 0.001 << " "
                                   << vertices[i].y * 0.001 << " "
                                   << vertices[i].z * 0.001 << " "
                                   << colors[i].r << " "
                                   << colors[i].g << " "
                                   << colors[i].b << std::endl;

                        // TODO: could the check be possibly more elegant
                        if(texCoords.size() == vertices.size()){
                            ss_objs[m] << "vt " << std::fixed
                                       << texCoords[i].x << " "
                                       << texCoords[i].y << std::endl;
                        }

                        if(meshes[m].hasNormals()){
                            ss_objs[m] << "vn " << std::fixed
                                       << meshes[m].getNormals()[i].x << " "
                                       << meshes[m].getNormals()[i].y << " "
                                       << meshes[m].getNormals()[i].z << std::endl;
                        }
                    }

                });
            ss_threads[m * 2 + 1] = thread([&, m] {
                    string scopeName = "saving image " + ofToString(m);
                    OFX_PROFILER_SCOPE(scopeName.c_str());
                    string color_image_name = names[m] + "_tex_Color.png";
                    string color_image_path = dirString + "/" + color_image_name;
                    ofSaveImage(textures[m], color_image_path);
                });
        }

        // first, only join the string threads
        for(int i = 0; i < int(meshes.size()); i++){
            ss_threads[i * 2].join();
        }

        // prepare the strings
        stringstream ss_obj;
        stringstream ss_mtl;

        ss_obj << std::fixed << "# the combined points\n\n";
        ss_obj << std::fixed << "mtllib material.mtl\n";
        ss_obj << std::fixed << "o human\n\n";

        // concatenate them
        for(int i = 0; i < meshes.size(); i++){
            ss_obj << ss_objs[i].str();
            ss_mtl << ss_mtls[i].str();
        }

        // save the files
        ofFile obj;
        obj.open(dirString + "/model.obj", ofFile::WriteOnly);
        obj << std::fixed << ss_obj.str();
        obj.close();

        ofFile mtl;
        mtl.open(dirString + "/material.mtl", ofFile::WriteOnly);
        mtl << std::fixed << ss_mtl.str();
        mtl.close();

        // lastly, join the image saving threads
        for(int i = 0; i < int(meshes.size()); i++){
            ss_threads[i * 2 + 1].join();
        }
    }
    is_saving.store(false);
    is_done.store(true);
}

void Saviour::saveLODs(vector <ofVboMesh> meshes,
                       vector <string> levels,
                       string subDirectory,
                       string fileType){

    if(is_ready.load()){
        is_saving.store(true);
        is_ready.store(false);
        deed = std::thread(&Saviour::saveThreadLODs, this,  meshes, levels, subDirectory, fileType);
    }
}

void Saviour::saveThreadLODs(vector <ofVboMesh> meshes,
                             vector <string> levels,
                             string subDirectory,
                             string fileType){

    OFX_PROFILER_FUNCTION();
    if(fileType == "obj"){
        last_name_mtx.lock();
        last_name = subDirectory;
        last_name_mtx.unlock();
        string dirString = saveDirectory + "/" + subDirectory;

        ofDirectory dir(dirString);
        dir.create(true);

        stringstream ss_objs[meshes.size()];
        std::thread ss_threads[meshes.size()];
        for(int m = 0; m < int(meshes.size()); m++){
            ss_threads[m] = thread([&, m] {
                    string scopeName = "collect vertices " + ofToString(m);
                    OFX_PROFILER_SCOPE(scopeName.c_str());

                    ss_objs[m] << "# unit 1m" << "\n";
                    ss_objs[m] << "g group" << levels[m] << "\n";

                    vector <glm::vec3> vertices = meshes[m].getVertices();
                    vector <ofFloatColor> colors = meshes[m].getColors();

                    for(int i = 0; i < int(vertices.size()); i++){
                        ss_objs[m] << "v " << std::fixed
                                   << vertices[i].x * 0.001 << " "
                                   << vertices[i].y * 0.001 << " "
                                   << vertices[i].z * 0.001 << " "
                                   << colors[i].r << " "
                                   << colors[i].g << " "
                                   << colors[i].b << std::endl;

                        if(meshes[m].hasNormals()){
                            ss_objs[m] << "vn " << std::fixed
                                       << meshes[m].getNormals()[i].x << " "
                                       << meshes[m].getNormals()[i].y << " "
                                       << meshes[m].getNormals()[i].z << std::endl;
                        }
                    }

                });
        }

        // first, only join the string threads
        for(int i = 0; i < int(meshes.size()); i++){
            ss_threads[i].join();
        }

        // concatenate them
        for(int i = 0; i < meshes.size(); i++){
            // save the files
            ofFile obj;
            obj.open(dirString + "/model_" + levels[i] + ".obj", ofFile::WriteOnly);
            obj << std::fixed << ss_objs[i].str();
            obj.close();
        }

    }
    is_saving.store(false);
    is_done.store(true);
}

string Saviour::getLastName(){
    lock_guard <mutex> mtx(last_name_mtx);
    return last_name;
}

}
