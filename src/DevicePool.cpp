#include "DevicePool.h"

/*
 * In current implementations, creating a device pool happens in the main application.
 * Mostly the devices are collected in an std:vector, std::map or similar, and everything is pretty much
 * hardcoded for the occasion. Parameters are handled with another vector, with the same amount of elements, etc.
 *
 * This class here is an attempt to make that task simpler and cleaner.
 * The hope is, that it can handle pretty much everything higher level relating to the cameras.
 * E.g. adding a camera to the pool when it's plugged in and removing it when it fails or is unplugged.
 * Maybe it should also include higher level extrinsic calibration of the cameras, so how do they relate to each other in space.
 *
 * Note, this code should be device agnostic, so it shouldn't have to care about whether a device is a kinect, or a realsense.
*/


namespace ofxDepthDevice {
DevicePool::DevicePool(){
    device_count_k4a = 0;
    updated_devices = false;
}
DevicePool::~DevicePool(){
    exit();
}

void DevicePool::setup(){
//    loadParameters("poolParameters.json");
    findConnectedDevices();

    parameters.add(pUpdateDevices.set("update_devices", true));
    parameters.add(pDrawDevices.set("draw_devices", true));
    parameters.add(pDrawDebug.set("draw_debug", false));
    parameters.add(pBlockOnSaving.set("block_on_saving", true));
//    parameters.add(pCenter.set("center", glm::vec3(0, 0, 0)));
//    parameters.add(pOrientation.set("orientation", glm::vec3(0, 0, 0)));
    parameters.setName("poolParameters");

    ofAddListener(parameters.parameterChangedE(), this, &DevicePool::poolParametersListener);

    for(int i = 0; i < PoolAction::NUM; i++){
        actions.add(ofParameter <bool>().set(PoolActionLabels[i], false));
        PoolActionLabelsReversed[PoolActionLabels[i]] = PoolAction(i); // lookup table
    }

    actions.getBool(PoolActionLabels[PoolAction::IDLE]).set(true);

    actions.setName("poolActions");

    ofAddListener(actions.parameterChangedE(), this, &DevicePool::poolActionsListener);

    // create the boards
    int nBoards = 6;
    vector <CalibrationHelper::CalibrationBoardParameters> cbps(nBoards, CalibrationHelper::CALIBRATIONBOARD_PARAMETERS_A2_MULTI);
    for(int i = 0; i < nBoards; i++){
        int markersPerBoard = (cbps[i].squaresX * cbps[i].squaresY) / 2;             // TODO: potential error on odd numbers
        cbps[i].offset += markersPerBoard * i;
    }
    // setup calibration thread
    CalibrationHelper::MultiHelper::setupStatic(cbps);
    auto & ueberCalibrationWorker = CalibrationHelper::MultiHelper::ueberCalibrationWorker;
    auto & is_calibrating_extrinsics = CalibrationHelper::MultiHelper::sUeberCalibrating;
    is_calibrating_extrinsics.store(false);
    ueberCalibrationWorker.setup();
    ueberCalibrationWorker.worker = std::thread([&] {
            using namespace CalibrationHelper;
            while(ueberCalibrationWorker.is_thread_running){
                while(is_calibrating_extrinsics.load()){
                    {
                        OFX_PROFILER_SCOPE("UEBER_CALIBRATION");
                        const auto & lastResults = ueberCalibrationWorker.getCurrentWriteBuffer();
                        auto & results = ueberCalibrationWorker.getNextWriteBuffer();
                        bool updated = false;
                        for(auto & ch_pair : calibrationHelpers){
                            int i = ch_pair.first;
                            string scope = "UBER_CALIBRATION helper " + ofToString(i);
                            OFX_PROFILER_SCOPE(scope.c_str());
                            auto & helper = ch_pair.second;
                            if(helper->calibrationWorker.isFresh(MultiHelper::CR_UEBER)){
                                auto & intermediate = helper->calibrationWorker.getFreshReadBuffer(MultiHelper::CR_UEBER);
                                MultiHelper::ExtrinsicsCalibrationResult result;
                                result.debug_image = std::move(intermediate.debug_image);
                                result.debug_pixels_of = std::move(intermediate.debug_pixels_of);
                                results[i] = std::move(result);
                                updated = true;
                            }else if(lastResults.find(i) != lastResults.end()){
                                results[i] = lastResults.at(i);
                            }
                        }
                        if(updated){
                            ueberCalibrationWorker.setFresh();
                        }
                    }
                    std::this_thread::sleep_for(60ms);
                }
                std::this_thread::sleep_for(60ms);
            }
        });
}

void DevicePool::poolParametersListener(ofAbstractParameter & e){
    if(e.getName() == "update_devices"){
        bool v = e.cast <bool>();
        for(auto & devicePair : connectedDevices){
            devicePair.second->setUpdating(v);
        }
    }
    if(e.getName() == "draw_devices"){
        bool v = e.cast <bool>();
        for(auto & dp : deviceParameters){
            dp.second.getBool("draw").set(v);
        }
    }
    if(e.getName() == "draw_debug"){
        bool v = e.cast <bool>();
        for(auto & dp : deviceParameters){
            dp.second.getBool("drawDebug").set(v);
        }
    }
}

// this is handy for one-shot switches
// we use these only for switching globally, the device actions are handled in update
void DevicePool::poolActionsListener(ofAbstractParameter & e){
    bool v = e.cast <bool>();
    ofLogNotice("poolActionlistener") << e.getName() << (v ? " is true" : " is false");
    if(v){
        for(auto label : PoolActionLabels){
            if(label == e.getName()){
                poolAction = PoolActionLabelsReversed[e.getName()];

                // in case we want to block updating during saving
                // NOTE: this might not immediately stop and go through a couple of more frames
                if(poolAction == PoolAction::SAVE_MODEL && pBlockOnSaving.get()){
                    for(auto & devicePair : connectedDevices){
                        devicePair.second->setUpdating(false);
                    }
                }else if(poolAction == PoolAction::EXTRINSIC_CALIBRATION_POSE){
                    auto & is_calibrating_extrinsics = CalibrationHelper::MultiHelper::sUeberCalibrating;
                    is_calibrating_extrinsics.store(true);
                }
            }else{
                actions.getBool(label).set(false);
            }
        }
    }else{
        if(PoolActionLabelsReversed[e.getName()] == PoolAction::EXTRINSIC_CALIBRATION_POSE){
            auto & is_calibrating_extrinsics = CalibrationHelper::MultiHelper::sUeberCalibrating;
            is_calibrating_extrinsics.store(false);
        }

        // set to idle if nothing else is doing anything
        bool anythingOn = false;
        for(auto label : PoolActionLabels){
            anythingOn = actions.getBool(label) ? true : anythingOn;
        }
        if(!anythingOn){
            actions.getBool(PoolActionLabels[PoolAction::IDLE]).set(true);
        }
    }
}

void DevicePool::update(){
    saviour.update();
    for(auto & devicePair : connectedDevices){
        auto & device = devicePair.second;
        auto & i = devicePair.first;
        device->update();

        // is initialisation done
        if(device->is_initialized){

            // calibration is not set up yet for camera
            if(calibrationHelpers.find(i) == calibrationHelpers.end()){
                calibrationHelpers[i] = make_shared <CalibrationHelper::MultiHelper>();

                calibrationHelpers[i]->setup(device->getSerialNumber());

//                // read intrinsics
//                calibrationHelpers[i]->intrinsics.isCalibrated =
//                    device->readIntrinsics(calibrationHelpers[i]->intrinsics.cameraMatrix,
//                                           calibrationHelpers[i]->intrinsics.distCoeffs);

                auto & extrinsicsWorker = calibrationHelpers[i]->calibrationWorker;
                extrinsicsWorker.setup();
                extrinsicsWorker.worker = std::thread([&, i, device] {
                        using namespace CalibrationHelper;
                        while(extrinsicsWorker.is_thread_running){
                            while(MultiHelper::sUeberCalibrating.load()){
                                string scope = "CALIBRATION helper " + ofToString(i);
                                OFX_PROFILER_SCOPE(scope.c_str());
                                auto & writer = extrinsicsWorker.getNextWriteBuffer();
                                const cv::Mat & mat = device->getCalibrationColorCv();
                                if(calibrationHelpers[i]->getBoardPoses(mat, writer.debug_image)){
                                    ofxCv::toOf(writer.debug_image, writer.debug_pixels_of);
                                    extrinsicsWorker.setFresh();
                                }
                                std::this_thread::sleep_for(20ms);
                            }
                            std::this_thread::sleep_for(10ms);
                        }
                    });
//                if(calibrationHelpers[i]->loadIntrinsicsFromDisk("intrinsics.yml")){
//                    // first manual intrinsics
//                }else{
//                    calibrationHelpers[i]->commonData.intrinsics.isCalibrated =
//                        device->readIntrinsics(calibrationHelpers[i]->commonData.intrinsics.cameraMatrix,
//                                               calibrationHelpers[i]->commonData.intrinsics.distCoeffs);
//                }

//                // read extrinsics
//                calibrationHelpers[i]->commonData.extrinsics.cameraTransformationMatrix = device->getColorToDepth();
//                calibrationHelpers[i]->commonData.extrinsics.hasTransformation = false;

//                if(calibrationHelpers[i]->loadExtrinsicsFromDisk("extrinsics.yml")){
//                }
            }

            // parameters are not set up yet for camera
            if(deviceParameters.find(i) == deviceParameters.end()){
                deviceParameters[i] = ofParameterGroup(device->getSerialNumber());
                deviceParameters[i].add(ofParameter <bool>().set("draw", true));
                deviceParameters[i].add(ofParameter <bool>().set("drawDebug", false));

                // TODO: updated devices is derived logic
                //       also not thread safe, but who cares atm
                updated_devices = true;
            }

        }
    }
    switch(poolAction){
     case PoolAction::SAVE_MODEL:
         if(saviour.is_ready && !saviour.is_done){
             vector <ofVboMesh> meshes;
             vector <ofPixels> textures;
             vector <string> names;
             for(auto & devicePair : connectedDevices){
                 if(devicePair.second->is_initialized
                    && deviceParameters[devicePair.first].getBool("draw")){
                     ofVboMesh mesh = devicePair.second->getVboMesh();
                     ofPixels texture(devicePair.second->getColorImage().getPixels());
                     string name = devicePair.second->getSerialNumber();
                     meshes.push_back(mesh);
                     textures.push_back(texture);
                     names.push_back(name);
                 }
             }
             string timestamp = ofGetTimestampString("%Y.%m.%d_%H:%M:%S");
             saviour.save(meshes, names, textures,timestamp);
         }else if(saviour.is_done){
             actions.getBool(PoolActionLabels[PoolAction::IDLE]).set(true);

             // in case we blocked updating and were updating before, undo it
             if(pUpdateDevices.get() && pBlockOnSaving.get()){
                 for(auto & devicePair : connectedDevices){
                     devicePair.second->setUpdating(true);
                 }
             }
             saviour.is_done.store(false);
         }
         break;

     case PoolAction::EXTRINSIC_CALIBRATION_POSE:
         break;


     case PoolAction::IDLE:
         break;


     default:
         break;

    }
}

void DevicePool::draw(){
    ofEnableDepthTest();
    for(auto & devicePair : connectedDevices){
        const int & i = devicePair.first;
        if(deviceParameters[i].getBool("draw").get()){
            devicePair.second->draw();
        }
    }
    ofDisableDepthTest();
}
void DevicePool::drawDebug(ofRectangle rect){
    using namespace CalibrationHelper;
    auto & ueberCalibrationWorker = MultiHelper::ueberCalibrationWorker;
    if(ueberCalibrationWorker.FRESH_INDEX.load() != -1){
        auto & reader = ueberCalibrationWorker.getFreshReadBuffer(MultiHelper::UCR_DRAW);
        for(auto & resultPair : reader){
            const int & i = resultPair.first;
            if(connectedDevices[i]->is_initialized && deviceParameters[i].getBool("drawDebug")){
                auto & result = resultPair.second;
                ofImage lorl;
                lorl.setFromPixels(result.debug_pixels_of);

                float w = rect.width;
                float h = (lorl.getHeight() * w / lorl.getWidth());
                float x = rect.x;
                float y = rect.y + (i * h) + 20;

                lorl.setUseTexture(true);
                lorl.update();
                lorl.draw(x, y, w, h);
                ofPushStyle();
                ofNoFill();
                int r = ofRandom(0, 255);
                ofSetColor(ofColor(r));
                ofSetLineWidth(10);
                ofDrawRectangle(x, y, w, h);
                ofPopStyle();
            }
        }
    }
}

void DevicePool::exit(){
    for(auto & devicePair : connectedDevices){
        devicePair.second->close();
    }
    connectedDevices.clear();
}

// the goal of this function is to detect whether a new device has been plugged in,
// or one has been plugged out (or failed for some reason), without crashing the application.
// Ideally we'd be able hotplug camera's at will.
// Doesn't work yet.
// we would need multiple functions for this:
// 1 count detected kinects
// 2 count initialized kinects
// 3 count currently initializing kinects
// 4 detected - (initialized + initializing) = to be initialized
// 5 initialize to be initialized
void DevicePool::findConnectedDevices(){
    #ifdef K4A
        const int32_t device_count = k4a_device_get_installed_count();

        if(device_count != device_count_k4a){
            for(int i = 0; i < device_count; i++){
                // initialize them
                auto kinect = make_shared <ofxDepthDevice::Kinect>();
                kinect->setup(i, ofxDepthDevice::K4A_DEVICE_CONFIG_OFXK4AMINI_DEFAULT);
                connectedDevices[i] = kinect;

                device_count_k4a++;
                cout << "finding device i: " << i << endl;
            }
        }
    #endif
}
}
