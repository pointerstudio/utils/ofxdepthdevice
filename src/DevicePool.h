#ifndef DEVICEPOOL_H
#define DEVICEPOOL_H

#include "Util.h"
#include "DepthDevice.h"
#include "Kinect.h"
#include "Saviour.h"

namespace ofxDepthDevice {

class DevicePool {
    public:
        enum PoolAction {
            IDLE = 0,
            GENERATE_MARKER,
            INTRINSIC_CALIBRATION_COLLECT,
            INTRINSIC_CALIBRATION_CALIBRATE,
            EXTRINSIC_CALIBRATION_POSE,
            SAVE_MODEL,
            NUM
        };

        string PoolActionLabels[6] = {
            "IDLE",
            "GENERATE_MARKER",
            "INTRINSIC_CALIBRATION_COLLECT",
            "INTRINSIC_CALIBRATION_CALIBRATE",
            "EXTRINSIC_CALIBRATION_POSE",
            "SAVE_MODEL"
        };
        unordered_map <string, PoolAction> PoolActionLabelsReversed;

        DevicePool();
        ~DevicePool();
        void setup();
        void update();
        void findConnectedDevices();
        void draw();
        void drawDebug(ofRectangle rect);
        void exit();

        void loadParameters(string settingsFilePath);

        void poolParametersListener(ofAbstractParameter & e);
        void poolActionsListener(ofAbstractParameter & e);

        void startExtrinsicCalibration();

        ofParameter <bool> pUpdateDevices;
        ofParameter <bool> pDrawDevices;
        ofParameter <bool> pDrawDebug;
        ofParameter <bool> pBlockOnSaving;
        ofParameter <glm::vec3> pCenter;
        ofParameter <glm::vec3> pOrientation;
        ofParameterGroup parameters;

        ofParameter <bool> pSaveModel;
        ofParameterGroup actions;
        PoolAction poolAction = PoolAction::IDLE;

        map <int, shared_ptr <DepthDevice> > connectedDevices;
        map <int, ofParameterGroup> deviceParameters;
        map <int, shared_ptr <CalibrationHelper::MultiHelper> > calibrationHelpers;

        bool updated_devices;

        int device_count_k4a;

        Saviour saviour;
};
}

#endif // DEVICEPOOL_H
