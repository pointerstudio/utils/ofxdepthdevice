#ifndef SERIALRELAY_H
#define SERIALRELAY_H

#include "ofMain.h"

#define BAUD 57600
#define MARKER_START 'a'
#define MARKER_STOP 'z'
#define HOST_COMMAND_ON 'x'
#define HOST_COMMAND_OFF 'y'
#define RESET_TIMER 5000000

/*
 * this class is compatible with
 * the arduino file in ofxDepthDevice/resources/arduino/serialrelays/serialrelays.ino
 */

class SerialRelay {
    public:
        SerialRelay();
        void setup();
        void setup(int deviceId);
        void set(unsigned char cmd);
        void reset();
        ofSerial serial;
};

#endif // SERIALRELAY_H
