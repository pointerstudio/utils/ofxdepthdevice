#!/usr/bin/python
import glob, os, errno, shutil

################################################################################################# UTILS

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno == 21: # 21 = is a directory
            shutil.rmtree(filename)
        elif e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred

################################################################################################# MAIN
DIR = os.path.dirname(os.path.abspath(__file__))
ADDON_NAME = os.path.basename(DIR)

print('shall we remove all qtcreator build directories build-* as well?')
removeBuildDirectories = raw_input('(y/n) --> ')
if removeBuildDirectories == "y":
    for exampleDir in glob.glob(r'./build-*'):
        print('removing {}'.format(exampleDir))
        silentremove(exampleDir)

for exampleDir in glob.glob(r'./example-*'):
    print('cleaning {}'.format(exampleDir))
    silentremove(exampleDir + '/Makefile')
    silentremove(exampleDir + '/config.make')
    silentremove(exampleDir + '/obj')
    for filepath in glob.glob(exampleDir + '/*.qbs*'):
        silentremove(filepath)

print('cleaned examples. Should we also use projectGenerator to create new project files?')
useProjectGenerator = raw_input('(y/n) --> ')
if useProjectGenerator == "y":
    for exampleDir in glob.glob(r'./example-*'):
        addonsMake = exampleDir + '/addons.make'
        if os.path.isfile(addonsMake):
            with open(addonsMake) as f:
                addons = f.readlines()
                addons = [x.strip() for x in addons]
            
                cmd = 'projectGenerator -a"{}" {}'.format(', '.join(addons), exampleDir)
                os.system(cmd)
        else:
            cmd = 'projectGenerator -a"{}" {}'.format(ADDON_NAME, exampleDir)
            os.system(cmd)

print("""
                       ______
                      /\     \ 
                     /  \  I  \ 
                    /    \_____\ 
                   _\    / ____/_
                  /\ \  / /\     \ 
                 /  \ \/_/  \  T  \ 
                /    \__/    \_____\ 
               _\    /  \    / ____/_
              /\ \  /    \  / /\     \ 
             /  \ \/_____/\/_/  \     \ 
            /    \_____\    /    \_____\ 
           _\    /     /    \    / ____/_
          /\ \  /  I  /      \  / /\     \ 
         /  \ \/_____/        \/_/  \     \ 
        /    \_____\            /    \_____\ 
       _\    /     /            \    / ____/_
      /\ \  /  S  /              \  / /\     \ 
     /  \ \/_____/                \/_/  \     \ 
    /    \_____\                    /    \_____\ 
   _\    /     /_  ______  ______  _\____/ ____/_
  /\ \  /     /  \/\     \/\     \/\     \/\     \ 
 /  \ \/_____/    \ \     \ \     \ \     \ \     \ 
/    \_____\ \_____\ \_____\ \_____\ \_____\ \_____\ 
\    /     / /     / /     / /     / /     / /     /
 \  /     / /  D  / /  O  / /  N  / /  E  / /     /
  \/_____/\/_____/\/_____/\/_____/\/_____/\/_____/ 
  
  """)
