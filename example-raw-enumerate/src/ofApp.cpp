#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    k4a_device_t device = NULL;
    int device_count = k4a_device_get_installed_count();

    ofLogNotice("setup", "found %d connected devices", device_count);

    for(int deviceIndex = 0; deviceIndex < device_count; deviceIndex++){
        if(K4A_RESULT_SUCCEEDED == k4a_device_open(deviceIndex, &device)){
            ofLogNotice("setup", "opened device %d", deviceIndex);
        }else{
            ofLogError("setup", "failed opening device %d", deviceIndex);
        }
        char * serial_number = NULL;
        size_t serial_number_length = 0;

        if(K4A_BUFFER_RESULT_TOO_SMALL != k4a_device_get_serialnum(device, NULL, &serial_number_length)){
            ofLogError("setup", "failed getting serial number length %d", deviceIndex);
            k4a_device_close(device);
            device = NULL;
            continue;
        }

        serial_number = (char *)malloc(serial_number_length);
        if(serial_number == NULL){
            ofLogError("setup", "%d: Failed to allocate memory for serial number (%zu bytes)\n", deviceIndex, serial_number_length);
            k4a_device_close(device);
            device = NULL;
            continue;
        }

        if(K4A_BUFFER_RESULT_SUCCEEDED != k4a_device_get_serialnum(device, serial_number, &serial_number_length)){
            ofLogError("Setup", "%d: Failed to get serial number\n", deviceIndex);
            free(serial_number);
            serial_number = NULL;
            k4a_device_close(device);
            device = NULL;
            continue;
        }

        ofLogNotice("setup", "%d: Device \"%s\"\n", deviceIndex, serial_number);

        free(serial_number);
        serial_number = NULL;
        k4a_device_close(device);
        device = NULL;
    }
    ofExit(0);
}
