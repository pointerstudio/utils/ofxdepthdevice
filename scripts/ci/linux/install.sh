#!/bin/bash

# License
#
# Copyright (c) 2020 Jakob Schloetter studio@pointer.click
# Distributed under the MIT License.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#


########################
# install k4a on linux #
########################

# first, where are we
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CURRENT_DIR=$(pwd)

############
# user defined variables

INSTALL_DIR=$(pwd)/install
TMP_DIR=/tmp/installk4a
INSTALL_SDK=NO
INSTALL_OFX=YES
AUTOPILOT=NO
NO_INSTALLATION=NO
TEST_INSTALLATION=YES

#make it beautiful
cd $SCRIPT_DIR/../../../libs
OFX_INSTALL_DIR=$(pwd)
cd $SCRIPT_DIR


############
# utility functions

PROGRESS_COMMENT () {
    echo ""
    echo "########################"
    echo "# install k4a on linux #"
    echo "########################"
    echo "-> ${1}"
    echo ""
}

############
# arguments

PROGRESS_COMMENT "parse arguments"
for i in "$@"
do
case $i in
    -i=*|--install-dir=*)
    INSTALL_DIR="${i#*=}"
    shift
    ;;
    -t=*|--tmp-dir=*)
    TMP_DIR="${i#*=}"
    shift
    ;;
    --sdk)
    INSTALL_SDK=YES
    shift
    ;;
    --nofx)
    INSTALL_OFX=NO
    shift
    ;;
    --noauto)
    AUTOPILOT=NO
    shift
    ;;
    --noinstall)
    NO_INSTALLATION=YES
    shift
    ;;
    --notest)
    TEST_INSTALLATION=NO
    shift
    ;;
    -h|--help)
    cat $0
    echo ""
    echo "if something is unclear, check the script content"
    exit 1
    ;;
    *)
          # unknown option
    ;;
esac
done

PROGRESS_COMMENT "verification of user defined variables"
echo "INSTALL_DIR: ${INSTALL_DIR}"
echo "TMP_DIR: ${TMP_DIR}"
echo "OFX_INSTALL_DIR: ${OFX_INSTALL_DIR}"
echo "INSTALL_OFX: ${INSTALL_OFX}"
echo "INSTALL_SDK: ${INSTALL_SDK}"
echo "AUTOPILOT: ${AUTOPILOT}"
echo "NO_INSTALLATION: ${NO_INSTALLATION}"
echo "TEST_INSTALLATION: ${TEST_INSTALLATION}"
echo ""
if [[ $AUTOPILOT = NO ]]
then
    echo "does this sound alright?"
    read -p "(y/n) " -n 1 -r
    echo ""
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo "alright"
    else
        echo "you may want to adjust variables in ${0}, or pass them as arguments"
        exit 1
    fi
fi

clean_preinstall() {
    rm -rf ${OFX_INSTALL_DIR}/*k4a*
    rm -rf ${TMP_DIR}/*
    rm -rf ${INSTALL_DIR}/*
}

install_dependencies () {
    echo "install dependencies (libsoundio-dev)?"
    read -p "(y/n) " -n 1 -r
    echo ""
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo "alright"
    	if [[ $(command -v apt) ]];
    	then
    	    sudo apt install libsoundio-dev
    	elif [[ $(command -v pamac) ]];
    	then
    	    pamac install libsoundio
    	    sudo ln -s /usr/lib/libsoundio.so.2 /usr/lib/libsoundio.so.1
    	else
    	    echo "please make sure libsoundio is installed"
    	fi
    else
        echo "okay, not installing dependencies"
    fi
}

install_udev () {
    online_md5="$(curl -sL https://raw.githubusercontent.com/microsoft/Azure-Kinect-Sensor-SDK/develop/scripts/99-k4a.rules | md5sum | cut -d ' ' -f 1)"
    local_md5="$(md5sum /etc/udev/rules.d/99-k4a.rules | cut -d ' ' -f 1)"
    
    if [ "$online_md5" == "$local_md5" ];
    then
        echo "udev rules up to date!"
    else
        echo "download and install udev rules"
        wget -O ./99-k4a.rules https://raw.githubusercontent.com/microsoft/Azure-Kinect-Sensor-SDK/develop/scripts/99-k4a.rules
        sudo mv ./99-k4a.rules /etc/udev/rules.d/99-k4a.rules
    fi
}

extract_deb () {
    DEB_FILE=$1
    X_TMP_DIR=${TMP_DIR}/x
    TARGET_DIR=$2
    mkdir -p ${X_TMP_DIR}
    echo "DEB_FILE |${DEB_FILE}|"
    echo $TARGET_DIR
    # check if we have dpkg for extracting
    # e.g. ubuntu
    if [[ $(command -v dpkg) ]];
    then
        echo "detected dkpg.. extracting deb file"
        dpkg -x ${DEB_FILE} ${TARGET_DIR}/
    else
        echo "no dkpg.. extracting deb file manually"
        ar p $DEB_FILE data.tar.gz | tar zx -C ${X_TMP_DIR}
        cp -r ${X_TMP_DIR}/usr ${TARGET_DIR}/
        rm -rf ${X_TMP_DIR}
    fi
}

install_k4a () {
    K4A_MAJOR=$1
    K4A_MINOR=$2
    K4A_PATCH=$3
    K4A_VERSION=${K4A_MAJOR}.${K4A_MINOR}
    K4A_LONGVERSION=${K4A_MAJOR}.${K4A_MINOR}.${K4A_PATCH}
    K4A_INSTALL_DIR=${INSTALL_DIR}/libk4a${K4A_VERSION}

    mkdir -p ${K4A_INSTALL_DIR}
    wget https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4a${K4A_VERSION}/libk4a${K4A_VERSION}_${K4A_LONGVERSION}_amd64.deb -P ${TMP_DIR}
    wget https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4a${K4A_VERSION}-dev/libk4a${K4A_VERSION}-dev_${K4A_LONGVERSION}_amd64.deb -P ${TMP_DIR}
    extract_deb ${TMP_DIR}/libk4a${K4A_VERSION}_${K4A_LONGVERSION}_amd64.deb ${K4A_INSTALL_DIR}
    extract_deb ${TMP_DIR}/libk4a${K4A_VERSION}-dev_${K4A_LONGVERSION}_amd64.deb ${K4A_INSTALL_DIR}
    #rm -rf ./libk4a${K4A_VERSION}*.deb
}

install_k4abt () {
    K4ABT_MAJOR=$1
    K4ABT_MINOR=$2
    K4ABT_PATCH=$3
    K4ABT_VERSION=${K4ABT_MAJOR}.${K4ABT_MINOR}
    K4ABT_LONGVERSION=${K4ABT_MAJOR}.${K4ABT_MINOR}.${K4ABT_PATCH}
    K4ABT_INSTALL_DIR=${INSTALL_DIR}/libk4abt${K4ABT_VERSION}

    mkdir -p ${K4ABT_INSTALL_DIR}
    wget https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4abt${K4ABT_VERSION}/libk4abt${K4ABT_VERSION}_${K4ABT_LONGVERSION}_amd64.deb -P ${TMP_DIR}
    wget https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/libk/libk4abt${K4ABT_VERSION}-dev/libk4abt${K4ABT_VERSION}-dev_${K4ABT_LONGVERSION}_amd64.deb -P ${TMP_DIR}
    extract_deb ${TMP_DIR}/libk4abt${K4ABT_VERSION}_${K4ABT_LONGVERSION}_amd64.deb ${K4ABT_INSTALL_DIR}
    extract_deb ${TMP_DIR}/libk4abt${K4ABT_VERSION}-dev_${K4ABT_LONGVERSION}_amd64.deb ${K4ABT_INSTALL_DIR}
}

install_k4a_tools () {
    K4A_MAJOR=$1
    K4A_MINOR=$2
    K4A_PATCH=$3
    K4A_VERSION=${K4A_MAJOR}.${K4A_MINOR}
    K4A_LONGVERSION=${K4A_MAJOR}.${K4A_MINOR}.${K4A_PATCH}
    K4A_TOOLS_INSTALL_DIR=${INSTALL_DIR}/k4a-tools-${K4A_VERSION}

    mkdir -p ${K4A_TOOLS_INSTALL_DIR}
    wget https://packages.microsoft.com/ubuntu/18.04/prod/pool/main/k/k4a-tools/k4a-tools_${K4A_LONGVERSION}_amd64.deb -P ${TMP_DIR}
    extract_deb ${TMP_DIR}/k4a-tools_${K4A_LONGVERSION}_amd64.deb ${K4A_TOOLS_INSTALL_DIR}
    # if you don't use ubuntu, just rename deb into zip, unzip, extract the tar.gz file,
    # and then just install everything in your install folder
    rm -rf ./k4a-tools_${K4A_LONGVERSION}_amd64.deb
}

install_ofx_k4a () {
    PROGRESS_COMMENT "install ofx k4a"
    K4A_MAJOR=$1
    K4A_MINOR=$2
    K4A_PATCH=$3
    K4A_VERSION=${K4A_MAJOR}.${K4A_MINOR}
    K4A_LONGVERSION=${K4A_MAJOR}.${K4A_MINOR}.${K4A_PATCH}
    LIB_NAME=libk4a
    K4A_INSTALL_DIR="${INSTALL_DIR}/${LIB_NAME}${K4A_VERSION}"
    OFX_DESTINATION="${OFX_INSTALL_DIR}/${LIB_NAME}"

    PROGRESS_COMMENT "install ofx ${LIB_NAME}"
    echo "OFX_DESTINATION: ${OFX_DESTINATION}"

    cp -r ${K4A_INSTALL_DIR}/usr ${OFX_DESTINATION}
    mv ${OFX_DESTINATION}/lib/x86_64-linux-gnu ${OFX_DESTINATION}/lib/linux64

    if [[ ${K4A_MAJOR} = 1 && ${K4A_MINOR} = 4 ]] ;
    then
        echo "create link from 1.4 to 1.3 for bodytracking"
        ln -s ${OFX_DESTINATION}/lib/linux64/libk4a.so.1.4 ${OFX_DESTINATION}/lib/linux64/libk4a.so.1.3
    fi
    echo "get libdepthengine in the right place"
    mv ${OFX_DESTINATION}/lib/linux64/libk4a${K4A_VERSION}/libdepthengine.so* ${OFX_DESTINATION}/lib/linux64/
}

install_ofx_k4abt () {
    K4ABT_MAJOR=$1
    K4ABT_MINOR=$2
    K4ABT_PATCH=$3
    K4ABT_VERSION=${K4ABT_MAJOR}.${K4ABT_MINOR}
    K4ABT_LONGVERSION=${K4ABT_MAJOR}.${K4ABT_MINOR}.${K4ABT_PATCH}
    LIB_NAME=libk4abt
    K4ABT_INSTALL_DIR=${INSTALL_DIR}/${LIB_NAME}${K4ABT_VERSION}
    OFX_DESTINATION=${OFX_INSTALL_DIR}/${LIB_NAME}

    PROGRESS_COMMENT "install ofx ${LIB_NAME}"

    cp -r ${K4ABT_INSTALL_DIR}/usr ${OFX_DESTINATION}

    mv ${OFX_DESTINATION}/lib ${OFX_DESTINATION}/libtmp
    mkdir -p ${OFX_DESTINATION}/lib/linux64
    mv ${OFX_DESTINATION}/libtmp/* ${OFX_DESTINATION}/lib/linux64/
    rm -rf ${OFX_DESTINATION}/libtmp
}

install_ofx_k4a_tools () {
    K4A_MAJOR=$1
    K4A_MINOR=$2
    K4A_PATCH=$3
    K4A_VERSION=${K4A_MAJOR}.${K4A_MINOR}
    K4A_LONGVERSION=${K4A_MAJOR}.${K4A_MINOR}.${K4A_PATCH}
    LIB_NAME=k4atools
    K4A_TOOLS_INSTALL_DIR=${INSTALL_DIR}/k4a-tools-${K4A_VERSION}
    OFX_DESTINATION=${OFX_INSTALL_DIR}/${LIB_NAME}

    PROGRESS_COMMENT "install ofx ${LIB_NAME}"

    cp -r ${K4A_TOOLS_INSTALL_DIR}/usr ${OFX_DESTINATION}
}

test_viewer () {
    K4A_MAJOR=$1
    K4A_MINOR=$2
    K4A_PATCH=$3
    K4A_VERSION=${K4A_MAJOR}.${K4A_MINOR}
    K4A_LONGVERSION=${K4A_MAJOR}.${K4A_MINOR}.${K4A_PATCH}
    K4A_INSTALL_DIR=${INSTALL_DIR}/libk4a${K4A_VERSION}
    K4A_TOOLS_INSTALL_DIR=${INSTALL_DIR}/k4a-tools-${K4A_VERSION}
    K4A_LIBS=${K4A_INSTALL_DIR}/usr/lib/x86_64-linux-gnu
    K4A_TOOLS_BIN=${K4A_TOOLS_INSTALL_DIR}/usr/bin

    export LD_LIBRARY_PATH=${K4A_LIBS}

    PROGRESS_COMMENT "test installation of libk4a ${K4A_VERSION}"
    read -p "test k4aviewer ${K4A_VERSION}? (y/n)" -n 1 -r
    echo ""
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        cd ${K4A_TOOLS_BIN}
        ./k4aviewer
    fi
}

test_bodytracking () {
    K4A_MAJOR=$1
    K4A_MINOR=$2
    K4A_PATCH=$3
    K4A_VERSION=${K4A_MAJOR}.${K4A_MINOR}
    K4A_LONGVERSION=${K4A_MAJOR}.${K4A_MINOR}.${K4A_PATCH}
    K4A_INSTALL_DIR=${INSTALL_DIR}/libk4a${K4A_VERSION}
    K4A_TOOLS_INSTALL_DIR=${INSTALL_DIR}/k4a-tools-${K4A_VERSION}
    K4ABT_MAJOR=$4
    K4ABT_MINOR=$5
    K4ABT_PATCH=$6
    K4A_LIBS=${K4A_INSTALL_DIR}/usr/lib/x86_64-linux-gnu
    K4A_TOOLS_BIN=${K4A_TOOLS_INSTALL_DIR}/usr/bin
    K4ABT_VERSION=${K4ABT_MAJOR}.${K4ABT_MINOR}
    K4ABT_INSTALL_DIR=${INSTALL_DIR}/libk4abt${K4ABT_VERSION}
    K4ABT_LIBS=${K4ABT_INSTALL_DIR}/usr/lib
    K4ABT_BIN=${K4ABT_INSTALL_DIR}/usr/bin

    if (( $K4A_MINOR > 3 )); then
        PROGRESS_COMMENT "warning!"
        echo "K4A-${K4A_VERSION} and K4ABT-${K4ABT_VERSION} officially not compatible"
        echo "creating symbolic link as a naive fix."
        echo "this worked for us but is not properly tested or thought through, no guarantees."
        ln -s ${K4A_LIBS}/libk4a.so.${K4A_LONGVERSION} ${K4A_LIBS}/libk4a.so.1.3
        ln -s ${K4A_LIBS}/libk4a.so.${K4A_LONGVERSION} ${K4A_LIBS}/libk4a.so.1.3.0
        # location of depthengine is changed with 1.4, so we need to add that directory to libs path
        K4A_LIBS=${K4A_LIBS}:${K4A_INSTALL_DIR}/usr/lib/x86_64-linux-gnu/libk4a${K4A_VERSION}
    fi

    export LD_LIBRARY_PATH=${K4A_LIBS}:${K4ABT_LIBS}:${K4ABT_BIN}

    PROGRESS_COMMENT "test installation of libk4abt ${K4ABT_VERSION} with libk4a ${K4A_VERSION}"
    read -p "test bodytracking ${K4ABT_VERSION} with libk4a-${K4A_VERSION}? (y/n)" -n 1 -r
    echo ""
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        cd ${K4ABT_BIN}
        ./k4abt_simple_3d_viewer
    fi

    # clean up
    if [[ $K4A_MINOR > 3 ]]; then
        echo "clean up"
        rm -rf ${K4A_INSTALL_DIR}/usr/lib/x86_64-linux-gnu/libk4a.so.1.3*
    fi
}

clean_preinstall

if [[ $NO_INSTALLATION = NO ]]
then
    install_dependencies

    install_udev

    install_k4a 1 4 1

    install_k4a_tools 1 4 1

    install_k4abt 1 0 0

    # yep, we could just install multiple versions in parallel
    # install_k4a 1 3 0
    # install_k4a_tools 1 3 0

    if [[ $INSTALL_OFX = YES ]]
    then

        install_ofx_k4a 1 4 1

        install_ofx_k4a_tools 1 4 1

        install_ofx_k4abt 1 0 0
        echo "installed the shit out of abt"

    fi
fi


if [[ $TEST_INSTALLATION = YES ]]
then
    #test_viewer 1 3 0
    #test_bodytracking 1 3 0 1 0 0 

    test_viewer 1 4 1

    test_bodytracking 1 4 1 1 0 0 

fi
